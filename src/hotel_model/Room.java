package hotel_model;

public class Room {
	private String room_id;
	private String room_type;
	private int room_rates;
	private RoomType type;
	public String getRoom_id() {
		return room_id;
	}
	public void setRoom_id(String room_id) {
		this.room_id = room_id;
	}
	public String getRoom_type() {
		return room_type;
	}
	public void setRoom_type(String room_type) {
		this.room_type = room_type;
	}
	public int getRoom_rates() {
		return room_rates;
	}
	public void setRoom_rates(int room_rates) {
		this.room_rates = room_rates;
	}
    public RoomType getType() {
        return type;
    }
    public void setType(RoomType type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        return room_id;
    }
    
    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(room_id);
    }
    
    public Room clone() {
        Room r = new Room();
        r.setRoom_id(this.getRoom_id());
        r.setRoom_rates(this.getRoom_rates());
        r.setRoom_type(this.getRoom_type());
        r.setType(this.getType().clone());
        return r;
    }
}

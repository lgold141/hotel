package hotel_model;

public class Booking {
    private String book_id;
    private Customer customer;
    private String customer_id;
    private String room_id;
    private Room room;
    private String date_arrival;
    private String date_leave;
    private String check_in;
    private String check_out;
    private int guests;
    public String getBook_id() {
        return book_id;
    }
    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public Room getRoom() {
        return room;
    }
    public void setRoom(Room room) {
        this.room = room;
    }
    public String getDate_arrival() {
        return date_arrival;
    }
    public String getDate_leave() {
		return date_leave;
	}
	public void setDate_leave(String date_leave) {
		this.date_leave = date_leave;
	}
	public void setDate_arrival(String date_arrival) {
        this.date_arrival = date_arrival;
    }
    public String getCheck_in() {
        return check_in;
    }
    public void setCheck_in(String check_in) {
        this.check_in = check_in;
    }
    public String getCheck_out() {
        return check_out;
    }
    public void setCheck_out(String check_out) {
        this.check_out = check_out;
    }
    public int getGuests() {
        return guests;
    }
    public void setGuests(int guests) {
        this.guests = guests;
    }
    public String getCustomer_id() {
        return customer_id;
    }
    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }
    public String getRoom_id() {
        return room_id;
    }
    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }
    
    public Booking clone() {
        Booking b = new Booking();
        b.setBook_id(this.getBook_id());
        b.setCheck_in(this.getCheck_in());
        b.setCheck_out(this.getCheck_out());
        b.setCustomer_id(this.getCustomer_id());
        b.setDate_arrival(this.getDate_arrival());
        b.setDateLeave(this.getDateLeave());
        b.setGuests(this.getGuests());
        b.setRoom_id(this.getRoom_id());
        b.setCustomer(this.getCustomer().clone());
        b.setRoom(this.getRoom().clone());
        return b;
    }
    public String getDateLeave() {
        return date_leave;
    }
    public void setDateLeave(String date_leave) {
        this.date_leave = date_leave;
    }
}

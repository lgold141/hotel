package hotel_model;


import java.util.Date;

import hotel_util.Messages;

public class Customer {
	private String customer_id;
	private String customer_name;
	private String gender;
	private Date date_bitrh;
	private String identity_card;
	private String nationality;
	private String phone_number;
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public Date getDate_bitrh() {
		return date_bitrh;
	}
	public void setDate_bitrh(Date date_bitrh) {
		this.date_bitrh = date_bitrh;
	}
	public String getIdentity_card() {
		return identity_card;
	}
	public void setIdentity_card(String identity_card) {
		this.identity_card = identity_card;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
    public String getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender = gender == 0 ? Messages.GENDER_FEMALE : Messages.GENDER_MALE;
    }

    public Customer clone() {
        Customer c = new Customer();
        c.setCustomer_id(this.getCustomer_id());
        c.setCustomer_name(this.getCustomer_name());
        c.setDate_bitrh(this.getDate_bitrh());
        c.setGender(this.getGender() == Messages.GENDER_FEMALE ? 0 : 1);
        c.setIdentity_card(this.getIdentity_card());
        c.setNationality(this.getNationality());
        c.setPhone_number(this.getPhone_number());
        return c;
    }
}

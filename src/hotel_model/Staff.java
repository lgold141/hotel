package hotel_model;

import java.util.Date;

import hotel_util.Messages;

public class Staff {
    private String id;
    private String name;
    private String pass;
    private int idCard;
    private Date birthDay;
    private String gender;
    private String address;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public int getIdCard() {
        return idCard;
    }
    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }
    public Date getBirthDay() {
        return birthDay;
    }
    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender = gender == 0 ? Messages.GENDER_FEMALE : Messages.GENDER_MALE;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String toString() {
        return name;
    }
    
    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(name);
    }
    
    public Staff clone() {
        Staff t =  new Staff();
        t.setAddress(this.getAddress());
        t.setBirthDay(this.getBirthDay());
        t.setGender(this.getGender() == Messages.GENDER_FEMALE ? 0 : 1);
        t.setId(this.getId());
        t.setIdCard(this.getIdCard());
        t.setName(this.getName());
        t.setPass(this.getPass());
        return t;
    }
}

package hotel_model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Bill {
    private String idBill;
    private String name;
    private String phone;
    private String idCard;
    private Date createdDate;
    private Staff staff;
    private Booking booking;
    private String roomType;
    private double roomPrice;
    private Date dateArrival;
    private Date dateLeave;
    private List<Benefit> benefits;
    private double totalPrice;
    public double getTotalPrice() {
        return totalPrice;
    }
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    public Date getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    public Staff getStaff() {
        return staff;
    }
    public void setStaff(Staff staff) {
        this.staff = staff;
    }
    public String getIdBill() {
        return idBill;
    }
    public void setIdBill(String idBill) {
        this.idBill = idBill;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getIdCard() {
        return idCard;
    }
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    public Booking getBooking() {
        return booking;
    }
    public void setBooking(Booking booking) {
        this.booking = booking;
    }
    public String getRoomType() {
        return roomType;
    }
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }
    public double getRoomPrice() {
        return roomPrice;
    }
    public void setRoomPrice(double roomPrice) {
        this.roomPrice = roomPrice;
    }
    public Date getDateArrival() {
        return dateArrival;
    }
    public void setDateArrival(Date dateArrival) {
        this.dateArrival = dateArrival;
    }
    public Date getDateLeave() {
        return dateLeave;
    }
    public void setDateLeave(Date dateLeave) {
        this.dateLeave = dateLeave;
    }
    public List<Benefit> getBenefits() {
        return benefits;
    }
    public void setBenefits(List<Benefit> benefits) {
        this.benefits = benefits;
    }
    @Override
    public String toString() {
        return "ID=" + idBill + " ,Name=" + name + " ,Price=" + roomPrice;
    }
    
    public Bill clone() {
        Bill b = new Bill();
        b.setCreatedDate(this.getCreatedDate());
        b.setDateLeave(this.getDateLeave());
        b.setDateArrival(this.getDateArrival());
        b.setIdBill(this.getIdBill());
        b.setIdCard(this.getIdCard());
        b.setName(this.getName());
        b.setPhone(this.getPhone());
        b.setRoomPrice(this.getRoomPrice());
        b.setRoomType(this.getRoomType());
        b.setTotalPrice(this.getTotalPrice());
        b.setStaff(this.getStaff().clone());
        b.setBooking(this.getBooking().clone());
        List<Benefit> bs = new ArrayList<Benefit>();
        for (Benefit benefit : this.getBenefits()) {
            bs.add(benefit.clone());
        }
        b.setBenefits(bs);
        return b;
    }
    
}

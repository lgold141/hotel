package hotel_model;

public class RoomType {
    private String typeId;
    private String typeName;
    private double price;
    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
    public String getTypeName() {
        return typeName;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    @Override
    public String toString() {
        return typeName;
    }
    
    @Override
    public boolean equals(Object obj) {
        return obj.toString().equals(typeName);
    }
    
    public RoomType clone() {
        RoomType r = new RoomType();
        r.setPrice(this.getPrice());
        r.setTypeId(this.getTypeId());
        r.setTypeName(this.getTypeName());
        return r;
    }
}

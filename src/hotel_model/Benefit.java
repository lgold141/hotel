package hotel_model;

import java.util.Date;

public class Benefit {
	private Integer benefit_id;
	private String benefit_name;
	private double benefit_price;
	private Date cusBenefitCreatedDay;
	public Date getCusBenefitCreatedDay() {
        return cusBenefitCreatedDay;
    }
    public void setCusBenefitCreatedDay(Date cusBenefitCreatedDay) {
        this.cusBenefitCreatedDay = cusBenefitCreatedDay;
    }
    public Integer getBenefitId() {
		return benefit_id;
	}
	public void setBenefitId(Integer benefit_id) {
		this.benefit_id = benefit_id;
	}
	public String getBenefitName() {
		return benefit_name;
	}
	public void setBenefitName(String benefit_name) {
		this.benefit_name = benefit_name;
	}
	
	public double getBenefitPrice() {
		return benefit_price;
	}
	public void setBenefitPrice(double benefit_price) {
		this.benefit_price = benefit_price;
	}
	@Override
	public String toString() {
		return "ID=" + benefit_id + " ,Name=" + benefit_name + " ,Price=" + benefit_price;
	}
	
	public Benefit clone() {
	    Benefit b = new Benefit();
	    b.setBenefitId(this.getBenefitId());
	    b.setBenefitName(this.getBenefitName());
	    b.setBenefitPrice(this.getBenefitPrice());
	    b.setCusBenefitCreatedDay(this.getCusBenefitCreatedDay());
	    return b;
	}
}

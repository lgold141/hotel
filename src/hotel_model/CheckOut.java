package hotel_model;

public class CheckOut {
	private int checkOutId;
	private String bookingId;
	private long roomPrice;
	private long servicesPrice;
	private String updateTime;

	public CheckOut(int checkOutId, String bookingId, long roomPrice, long servicesPrice, String updateTime) {
		super();
		this.checkOutId = checkOutId;
		this.bookingId = bookingId;
		this.roomPrice = roomPrice;
		this.servicesPrice = servicesPrice;
		this.updateTime = updateTime;
	}

	public int getCheckOutId() {
		return checkOutId;
	}

	public void setCheckOutId(int checkOutId) {
		this.checkOutId = checkOutId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public long getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(long roomPrice) {
		this.roomPrice = roomPrice;
	}

	public long getServicesPrice() {
		return servicesPrice;
	}

	public void setServicesPrice(long servicesPrice) {
		this.servicesPrice = servicesPrice;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
package hotel_manager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import hotel_connect.BookingService;
import hotel_connect.CustomerSQLService;
import hotel_connect.RoomSQLService;
import hotel_model.Booking;
import hotel_model.Customer;
import hotel_model.Room;

@SuppressWarnings("serial")
public class BookingUI extends JFrame {
	JTextField txtBookingId,txtRoomId,txtCustommerId,txtDateArrival, txtDateLeave, txtDateGuests,txtDateCheckIn,txtDateCheckOut;
	JComboBox cobRoom, cobCustomer;
	DefaultComboBoxModel model,model2;
	JButton btnAdd,btnDelete,btnEdit,btnSearch;
	DefaultTableModel dtmBooking;
	JTable tblBooking;
	Vector CB_Detail = new Vector();
	Vector CB_Customer = new Vector();
	List<Booking>listBooking=null;
	public BookingUI(String title ) {
		super(title);
		showCBBox();
		addControls();
		addEvents();
		showAllBooking();
		
	}

	private void showCBBox() {
		// TODO Auto-generated method stub
		RoomSQLService rss=new RoomSQLService();
		CB_Detail=rss.getListRoomId();
		System.out.println(CB_Detail);
		
		CustomerSQLService css=new CustomerSQLService();
		CB_Customer =css.getCustomerID();
		
	}

	private void showAllBooking() {
		// TODO Auto-generated method stub
		BookingService bkservice =new BookingService();
		listBooking=bkservice.getAllBooking();
		dtmBooking.setRowCount(0);
		for(Booking bk:listBooking) {
			Vector<Object>vec =new Vector<Object>();
			vec.add(bk.getBook_id());
			vec.add(bk.getCustomer_id());
			vec.add(bk.getRoom_id());
			vec.add(bk.getDate_arrival());
			vec.add(bk.getDate_leave());
			vec.add(bk.getGuests());
			dtmBooking.addRow(vec);
		}
	}

	private void addEvents() {
		// TODO Auto-generated method stub
		btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SearchUI ui =new SearchUI("Tìm kiếm đặt phòng");
				ui.showWin();
			}
		});
		btnAdd.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				addBookingUI();
			}
		});
	}

	protected void addBookingUI() {
		// TODO Auto-generated method stub
		BookingService bkservice =new BookingService();
		String bookingId = txtBookingId.getText();
		String roomId = (String) cobRoom.getSelectedItem();
		String customerId = (String) cobCustomer.getSelectedItem() ;
		String dateArival = txtDateArrival.getText();
		String dateLeave = txtDateLeave.getText();
		String checkIn = " null";
		String checkOut = " null";
		String guests = txtDateGuests.getText();
		System.out.println(roomId + customerId +bookingId + dateArival + dateLeave + checkIn + checkOut + guests);
		bkservice.addBooking(bookingId, customerId, roomId, dateArival, dateLeave, checkIn, checkOut, guests);
		showAllBooking();
	}

	private void addControls() {
		Container con=getContentPane();
		con.setLayout(new BorderLayout());
		// TODO Auto-generated method stub
		JPanel pnNorth =new JPanel();
		JPanel pnCenter =new JPanel();
		JPanel pnSouth =new JPanel();
		con.add(pnNorth,BorderLayout.NORTH);
		con.add(pnCenter,BorderLayout.CENTER);
		con.add(pnSouth,BorderLayout.SOUTH);
		
		pnNorth.setLayout(new BorderLayout());
		JPanel pnDetail=new JPanel();
		pnNorth.add(pnDetail,BorderLayout.CENTER);
		JPanel pnAction =new JPanel();
		pnNorth.add(pnAction,BorderLayout.EAST);
		
		pnDetail.setLayout(new BoxLayout(pnDetail, BoxLayout.Y_AXIS));
		JPanel pnBooking =new JPanel();
		JLabel lbBooking =new JLabel("Đặt phòng");
		lbBooking.setBackground(Color.BLUE);
		Font ft=new Font("arial",Font.BOLD,20);
		lbBooking.setFont(ft);
		pnBooking.add(lbBooking);
		pnDetail.add(pnBooking);
		
		JPanel pnBookingId =new JPanel();
		JLabel lbBoookingId =new JLabel("Mã đặt phòng");
		txtBookingId =new JTextField(25);
		pnBookingId.add(lbBoookingId);
		pnBookingId.add(txtBookingId);
		pnDetail.add(pnBookingId);
		
		JPanel pnRoomId =new JPanel();
		JLabel lbRoomId =new JLabel("Mã phòng");
		// cbRoom =new JComboBox(CB_Detail);
		model = new DefaultComboBoxModel(CB_Detail);
		cobRoom =new JComboBox(model);
		cobRoom.setPreferredSize(new Dimension(280,25));
		pnRoomId.add(lbRoomId);
		pnRoomId.add(cobRoom);
		pnDetail.add(pnRoomId);
		
		JPanel pnCustommerId =new JPanel();
		JLabel lbCustommerId =new JLabel("Mã khách hàng");
		model2 =new DefaultComboBoxModel(CB_Customer);
		cobCustomer=new JComboBox(model2);
		cobCustomer.setPreferredSize(new Dimension(280,25));
		// txtCustommerId =new JTextField(25);
		pnCustommerId.add(lbCustommerId);
		pnCustommerId.add(cobCustomer);
		pnDetail.add(pnCustommerId);
		
		JPanel pnDateArrival =new JPanel();
		JLabel lbDateArrival =new JLabel("Ngày đến");
		txtDateArrival =new JTextField(25);
		pnDateArrival.add(lbDateArrival);
		pnDateArrival.add(txtDateArrival);
		pnDetail.add(pnDateArrival);
		
		JPanel pnDateLeave =new JPanel();
		JLabel lbDateLeave =new JLabel("Ngày đi");
		txtDateLeave =new JTextField(25);
		pnDateLeave.add(lbDateLeave);
		pnDateLeave.add(txtDateLeave);
		pnDetail.add(pnDateLeave);
		
		JPanel pnDateGuests =new JPanel();
		JLabel lbDateGuests =new JLabel("Số lượng khách");
		txtDateGuests =new JTextField(25);
		pnDateGuests.add(lbDateGuests);
		pnDateGuests.add(txtDateGuests);
		pnDetail.add(pnDateGuests);
		
		pnAction.setLayout(new BoxLayout(pnAction, BoxLayout.Y_AXIS));
		JPanel JpBtnAdd =new JPanel();
		btnAdd=new JButton("Đặt phòng");
		JpBtnAdd.add(btnAdd);
		pnAction.add(JpBtnAdd);
		
		JPanel JpBtnDelete =new JPanel();
		btnDelete=new JButton("Xóa");
		JpBtnDelete.add(btnDelete);
//		pnAction.add(JpBtnDelete);
	
		JPanel JpBtnEdit =new JPanel();
		btnEdit=new JButton("Cập nhật");
		JpBtnEdit.add(btnEdit);
		pnAction.add(JpBtnEdit);
		
		pnCenter.setLayout(new BorderLayout());
		dtmBooking=new  DefaultTableModel();
		dtmBooking.addColumn("Mã đặt phòng");
		dtmBooking.addColumn("Mã khách hàng");
		dtmBooking.addColumn("Mã phòng");
		dtmBooking.addColumn("Ngày đến");
		dtmBooking.addColumn("Ngày đi");
		dtmBooking.addColumn("Số lượng khách");
		tblBooking =new JTable(dtmBooking);
		JScrollPane scBooking =new JScrollPane(tblBooking,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnCenter.add(scBooking,BorderLayout.CENTER);
		
		pnSouth.setLayout(new BorderLayout());
		btnSearch=new JButton("Tìm kiếm");
		Panel pnSearch =new Panel();
//		pnSearch.add(btnSearch);
		pnSouth.add(pnSearch);
		
		//Title
		TitledBorder borderBooking =new TitledBorder(BorderFactory.createLineBorder(Color.blue),"Thông tin chi tiết");
		pnDetail.setBorder(borderBooking);
		
		TitledBorder borderAction =new TitledBorder(BorderFactory.createLineBorder(Color.blue),"Thao tác");
		pnAction.setBorder(borderAction);
		
		TitledBorder borderTable =new TitledBorder(BorderFactory.createLineBorder(Color.blue),"Danh sách đặt phòng");
		pnCenter.setBorder(borderTable);
		
		//css 
		
		lbBoookingId.setPreferredSize(lbCustommerId.getPreferredSize());
		lbDateArrival.setPreferredSize(lbCustommerId.getPreferredSize());
//		lbDateCheckIn.setPreferredSize(lbCustommerId.getPreferredSize());
		lbDateLeave.setPreferredSize(lbCustommerId.getPreferredSize());
		lbRoomId.setPreferredSize(lbCustommerId.getPreferredSize());
		
		btnAdd.setIcon(new ImageIcon("image/them.png"));
		btnEdit.setIcon(new ImageIcon("image/sua.png"));
		btnDelete.setIcon(new ImageIcon("image/xoa_small.png"));
		btnSearch.setIcon(new ImageIcon("image/timkiem_small.png"));
		
		btnEdit.setPreferredSize(btnAdd.getPreferredSize());
		btnDelete.setPreferredSize(btnAdd.getPreferredSize());
		
	}
	public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}

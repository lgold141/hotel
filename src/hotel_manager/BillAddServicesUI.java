package hotel_manager;

import static hotel_util.ModelUtil.convertPrice;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import static hotel_util.Messages.*;
import static hotel_util.ModelUtil.*;
import hotel_interface.IBillService;
import hotel_model.Benefit;

public class BillAddServicesUI extends JFrame {

    private static final long serialVersionUID = -3460025871933263079L;
    private JLabel lblBillID;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JTable tableServices;
    private JButton btnCancel;
    private JButton btnFinish;
    private DefaultTableModel tableModel;
    private IBillService billService;
    private Map<Integer, Benefit> curBenefits;
    private Map<Integer, Benefit> benefits;
    private List<Benefit> newBenefits = new ArrayList<Benefit>();
    public static final Logger logger = Logger.getLogger(BillAddServicesUI.class);

    public BillAddServicesUI(IBillService service, Map<Integer, Benefit> map, String billID) {
        initializeUI(service, map, billID);
        initializeEvent();
    }

    public List<Benefit> getNewBenefits() {
        return newBenefits;
    }

    private void initializeEvent() {
        btnCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                closeWindow();
            }
        });

        btnFinish.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addBenefit();
            }
        });

        tableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent event) {
                logger.info("Listen table changed");
                if (event.getType() == TableModelEvent.UPDATE) {
                    if (event.getFirstRow() == event.getLastRow()) {
                        reflectData(event.getFirstRow());
                    } else {
                        reflectData(null);
                    }
                }
            }
        });
    }

    protected void addBenefit() {
        int[] rows = tableServices.getSelectedRows();
        int length = rows.length;
        if (length > 0) {
            if (length == 1) {
                try {
                    for (int row : rows) {
                        Integer id = (Integer) tableServices.getValueAt(row, 0);
                        newBenefits.add(benefits.get(id));
                        tableModel.removeRow(row);
                        tableModel.fireTableRowsDeleted(row, row);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            } else {
                NotificationUI.warningDialog(this, UPDATE_ADD_SER_BILL_TITLE, BENEFIT_SELECTED_SINGLE_ROW);
            }
        } else {
            NotificationUI.warningDialog(this, UPDATE_ADD_SER_BILL_TITLE, BENEFIT_SELECTED_SINGLE_ROW);
        }
    }

    protected void closeWindow() {
        this.dispose();
    }

    private void initializeUI(IBillService service, Map<Integer, Benefit> map, String billID) {
        this.billService = service;
        benefits = convertBenefitToMap(billService.getAllBenefits());

        JLabel jLabel5 = new JLabel();
        lblBillID = new JLabel();
        jPanel1 = new JPanel();
        jScrollPane1 = new JScrollPane();
        tableServices = new JTable();
        btnCancel = new JButton();
        btnFinish = new JButton();

        jLabel5.setText("jLabel5");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        if (billID != null) {
            lblBillID.setText(billID);
        }
        if (map != null) {
            this.curBenefits = map;
        }
        lblBillID.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N

        jPanel1.setBorder(BorderFactory.createTitledBorder("Benefits"));
        tableModel = new DefaultTableModel(0, 0);
        tableModel.setColumnIdentifiers(HEADERS_BENEFIT);
        tableServices.setModel(tableModel);
        reflectData(null);
        jScrollPane1.setViewportView(tableServices);

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
                        .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 653, Short.MAX_VALUE).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(
                GroupLayout.Alignment.TRAILING,
                jPanel1Layout.createSequentialGroup().addContainerGap()
                        .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        btnCancel.setText("Cancel");

        btnFinish.setText("Finish");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblBillID, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(btnFinish, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE)
                                .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addGap(18, 18, 18).addComponent(lblBillID)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnFinish)
                        .addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        pack();
        setLocation(600, 300);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void reflectData(Integer isUpdateRow) {
        List<Integer> ids = new ArrayList<Integer>();
        Enumeration enums = tableModel.getDataVector().elements();
        while (enums.hasMoreElements()) {
            Vector<Object> vector = (Vector<Object>) enums.nextElement();
            ids.add((Integer) vector.get(0));
        }
        if (isUpdateRow != null) {
        } else {
            for (Entry<Integer, Benefit> bf : benefits.entrySet()) {
                Integer id = bf.getKey();
                if (curBenefits != null) {
                    if (!ids.contains(id) && !curBenefits.containsKey(id)) {
                        Vector<Object> vector = new Vector<Object>();
                        vector.add(id);
                        vector.add(bf.getValue().getBenefitName());
                        vector.add(convertPrice(bf.getValue().getBenefitPrice()));
                        tableModel.addRow(vector);
                    }
                } else {
                    if (!ids.contains(id)) {
                        Vector<Object> vector = new Vector<Object>();
                        vector.add(id);
                        vector.add(bf.getValue().getBenefitName());
                        vector.add(convertPrice(bf.getValue().getBenefitPrice()));
                        tableModel.addRow(vector);
                    }
                }

            }
        }
    }
}

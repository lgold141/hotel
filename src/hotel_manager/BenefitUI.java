package hotel_manager;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import hotel_interface.IBenefitService;
import hotel_interface.ImBenefitService;
import hotel_model.Benefit;
import static hotel_util.Messages.*;
import static hotel_util.ModelUtil.convertPrice;

public class BenefitUI extends JFrame {

    private JFrame frame;
    private JButton btnNew, btnModify, btnDelete, btnClose;
    private Container cards;
    private JScrollPane jScrollPane2;
    private JTable table;
    final static String BUTTONPANEL = "Card with JButtons";
    final static String TEXTPANEL = "Card with JTextField";
    private DefaultTableModel model;
    private IBenefitService service;
    public static final Logger logger = Logger.getLogger(BenefitUI.class);

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BenefitUI window = new BenefitUI();
                    window.frame.setSize(800,600);
                    window.frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    window.frame.setLocationRelativeTo(null);
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public BenefitUI() {
        service = new ImBenefitService();
        initializeUI();
        initializeEvents();
    }

    public void initializeEvents() {
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent event) {
                logger.info("Listen table changed");
                if (event.getType() == TableModelEvent.UPDATE) {
                    if (event.getFirstRow() == event.getLastRow()) {
                        reflectData(event.getFirstRow());
                    } else {
                        reflectData(null);
                    }
                }
            }
        });
        btnClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        btnNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openEditBenefitUI(null,null);
            }
        });
        btnModify.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                modifyBenefit();
            }
        });
        btnDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteBenefit();
            }
        });
    }

    protected void modifyBenefit() {
        int[] rows = table.getSelectedRows();
        int length = rows.length;
        if (length > 0) {
            if (length == 1) {
                try {
                    for (int row : rows) {
                        Integer id = (Integer) table.getValueAt(row, 0);
                        openEditBenefitUI(id,row);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            } else {
                NotificationUI.warningDialog(frame, BENEFIT_MODIFY, BENEFIT_SELECTED_SINGLE_ROW);
            }
        } else {
            NotificationUI.warningDialog(frame, BENEFIT_MODIFY, BENEFIT_NOT_SELECTED_ROW);
        }
    }

    protected void deleteBenefit() {
        int[] rows = table.getSelectedRows();
        if (rows.length > 0) {
            try {
                for (int i = rows.length-1; i >= 0 ; i--) {
                    int row = rows[i];
                    Integer id = (Integer) table.getValueAt(row, 0);
                    String name = (String) table.getValueAt(row, 1);
                    if (service.deleteBenefit(id) > 0) {
                        model.removeRow(row);
                        model.fireTableRowsDeleted(row, row);
                        NotificationUI.infoDialog(frame, BENEFIT_DELETE, BENEFIT_DELETE_SUCCESS + " " + name);
                    } else {
                        NotificationUI.warningDialog(frame, BENEFIT_DELETE, BENEFIT_DELETE_FAIL + " " + name);
                    }
                }
            } catch (Exception e) {
                NotificationUI.errorDialog(frame, BENEFIT_DELETE, BENEFIT_DELETE_FAIL);
                logger.error(e.getMessage(), e);
            }
        } else {
            NotificationUI.warningDialog(frame, BENEFIT_DELETE, BENEFIT_NOT_SELECTED_ROW);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void reflectData(Integer idUpdateRow) {
        List<Integer> ids = new ArrayList<Integer>();
        Enumeration enums = model.getDataVector().elements();
        while (enums.hasMoreElements()) {
            Vector<Object> vector = (Vector<Object>) enums.nextElement();
            ids.add((Integer) vector.get(0));
        }
        if (idUpdateRow != null) {
            model.removeRow(idUpdateRow);
            Benefit bf = service.getBenefit(ids.get(idUpdateRow));
            Vector<Object> vector = new Vector<Object>();
            vector.add(bf.getBenefitId());
            vector.add(bf.getBenefitName());
            vector.add(convertPrice(bf.getBenefitPrice()));
            model.insertRow(idUpdateRow, vector);
        } else {
            for (Benefit bf : service.getBenefits()) {
                Integer id = bf.getBenefitId();
                if (!ids.contains(id)) {
                    Vector<Object> vector = new Vector<Object>();
                    vector.add(id);
                    vector.add(bf.getBenefitName());
                    vector.add(convertPrice(bf.getBenefitPrice()));
                    model.addRow(vector);
                }
            }
        }
    }

    /**
     * Initialize the contents of the frame.
     */
    public void initializeUI() {
        frame = new JFrame(BENEFIT_TITLE);
        // frame.setLocation(200, 200);
        frame.setLocationRelativeTo(null);
        jScrollPane2 = new JScrollPane();
        table = new JTable();
        btnNew = new JButton();

        btnModify = new JButton();
        btnDelete = new JButton();
        btnClose = new JButton();

        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        model = new DefaultTableModel(0, 0);
        model.setColumnIdentifiers(HEADERS_BENEFIT);

        reflectData(null);

        table.setModel(model);
        jScrollPane2.setViewportView(table);

        btnNew.setText("Add");
        btnDelete.setText("Delete");
        btnModify.setText("Modify");
        btnClose.setText("Close");

        cards = frame.getContentPane();
        GroupLayout layout = new GroupLayout(cards);
        cards.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                layout.createSequentialGroup().addContainerGap()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 467,
                                                        Short.MAX_VALUE)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(33, 33, 33)
                                                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(
                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(btnModify, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(42, 42, 42).addComponent(btnClose,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE, 87,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 326,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(btnNew)
                        .addComponent(btnDelete).addComponent(btnModify).addComponent(btnClose))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        frame.pack();
        frame.setVisible(true);
    }

    protected void openEditBenefitUI(Integer id, Integer idUpdateRow) {
        EditBenefitUI window = new EditBenefitUI(service, id);
        window.addWindowFocusListener(new WindowFocusListener() {

            @Override
            public void windowLostFocus(WindowEvent arg0) {
                if (id == null) {
                    model.fireTableDataChanged();
                } else {
                    model.fireTableRowsUpdated(idUpdateRow, idUpdateRow);
                }
            }

            @Override
            public void windowGainedFocus(WindowEvent arg0) {
            }
        });
    }
    public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		// this.setVisible(true);
	}
    
}

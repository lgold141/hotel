package hotel_manager;

import static hotel_util.Messages.*;
import static hotel_util.ModelUtil.convertPrice;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import hotel_interface.IBillService;
import hotel_interface.ImBillService;
import hotel_model.Bill;
import hotel_model.Booking;
import hotel_model.Customer;

public class BillUI extends JFrame {
    private JFrame frame;
    private JButton btnModify, btnDelete, btnCancel;
    private Container container;
    private JScrollPane jScrollPane2;
    private JTable table;
    private JLabel lblTitle;
    private DefaultTableModel model;
    private IBillService service;
    private Map<String, Bill> bills = new HashMap<String, Bill>();
    private JLabel jLabel5;
    private JButton btnAdd;
    public static final Logger logger = Logger.getLogger(BillUI.class);

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BillUI window = new BillUI();
                    window.frame.setSize(800,600);
                    window.frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
                    window.frame.setLocationRelativeTo(null);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        });
    }

    public BillUI() {
        service = new ImBillService();
        initializeUI();
        initializeEvents();
    }

    public void initializeEvents() {
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent event) {
                logger.info("Listen table changed");
                if (event.getType() == TableModelEvent.UPDATE) {
                    if (event.getFirstRow() == event.getLastRow()) {
                        reflectData(event.getFirstRow());
                    } else {
                        reflectData(null);
                    }
                }
            }
        });
        btnAdd.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                addBill(null, null);
            }
        });
        btnCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        btnModify.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                modifyBill();
            }
        });
        btnDelete.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                deleteBill();
            }
        });
    }

    protected void addBill(String id, Integer idUpdateRow) {
        AddBillUI editBillUI = new AddBillUI(service);
        editBillUI.addWindowFocusListener(new WindowFocusListener() {

            @Override
            public void windowLostFocus(WindowEvent e) {
                model.fireTableDataChanged();
            }

            @Override
            public void windowGainedFocus(WindowEvent e) {
            }
        });
    }

    protected void modifyBill() {
        int[] rows = table.getSelectedRows();
        int length = rows.length;
        if (length > 0) {
            if (length == 1) {
                try {
                    for (int row : rows) {
                        String id = (String) table.getValueAt(row, 0);
                        openEditUI(id, row);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            } else {
                NotificationUI.warningDialog(frame, UPDATE_BILL_TITLE, BILL_SELECTED_SINGLE_ROW);
            }
        } else {
            NotificationUI.warningDialog(frame, UPDATE_BILL_TITLE, BILL_NOT_SELECTED_ROW);
        }
    }

    protected void deleteBill() {
        int[] rows = table.getSelectedRows();
        if (rows.length > 0) {
            try {
                for (int i = rows.length - 1; i >= 0; i--) {
                    int row = rows[i];
                    String id = (String) table.getValueAt(row, 0);
                    String name = (String) table.getValueAt(row, 1);
                    if (service.deleteBill(id) > 0) {
                        model.fireTableRowsDeleted(row, row);
                        model.removeRow(row);
                        NotificationUI.infoDialog(frame, BILL_DELETE, BILL_DELETE_SUCCESS + " " + name);
                    } else {
                        NotificationUI.warningDialog(frame, BILL_DELETE, BENEFIT_DELETE_FAIL + " " + name);
                    }
                }
            } catch (Exception e) {
                NotificationUI.errorDialog(frame, BILL_DELETE, BENEFIT_DELETE_FAIL);
                logger.error(e.getMessage(), e);
            }
        } else {
            NotificationUI.warningDialog(frame, BILL_DELETE, BILL_NOT_SELECTED_ROW);
        }
    }

    protected void openEditUI(String id, Integer idUpdateRow) {
        EditBillUI editBillUI = new EditBillUI(service, id, bills.get(id));
        editBillUI.addWindowFocusListener(new WindowFocusListener() {

            @Override
            public void windowLostFocus(WindowEvent e) {
                if (id == null) {
                    model.fireTableDataChanged();
                } else {
                    model.fireTableRowsUpdated(idUpdateRow, idUpdateRow);
                }
            }

            @Override
            public void windowGainedFocus(WindowEvent e) {
            }
        });
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void reflectData(Integer idUpdateRow) {
        List<String> ids = new ArrayList<String>();
        Enumeration enums = model.getDataVector().elements();
        while (enums.hasMoreElements()) {
            Vector<Object> vector = (Vector<Object>) enums.nextElement();
            ids.add((String) vector.get(0));
        }
        if (idUpdateRow != null) {
            String id = ids.get(idUpdateRow);
            Bill bf = service.getBill(id);
            if (bf != null) {
                model.removeRow(idUpdateRow);
                Vector<Object> vector = new Vector<Object>();
                vector.add(id);
                Booking bk = bf.getBooking();
                Customer ct = bk.getCustomer();
                vector.add(ct.getCustomer_name());
                vector.add(ct.getPhone_number());
                vector.add(bf.getRoomType());
                vector.add(convertPrice(bf.getTotalPrice()));
                model.insertRow(idUpdateRow, vector);
            }
        } else {
            bills = service.getAllBills();
            for (Entry<String, Bill> entry : bills.entrySet()) {
                Bill bf = entry.getValue();
                String id = bf.getIdBill();
                if (!ids.contains(id)) {
                    Vector<Object> vector = new Vector<Object>();
                    vector.add(id);
                    Booking bk = bf.getBooking();
                    Customer ct = bk.getCustomer();
                    vector.add(ct.getCustomer_name());
                    vector.add(ct.getPhone_number());
                    vector.add(bf.getRoomType());
                    vector.add(convertPrice(bf.getTotalPrice()));
                    model.addRow(vector);
                }
            }
        }
    }

    /**
     * Initialize the contents of the frame.
     */
    public void initializeUI() {
        jLabel5 = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();

        lblTitle = new JLabel();
        jScrollPane2 = new JScrollPane();
        table = new JTable();
        btnModify = new JButton();
        btnCancel = new JButton();
        btnDelete = new JButton();
        frame = new JFrame(BILL_TITLE);
        frame.setLocation(500, 500);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setText(BILL_TITLE);
        lblTitle.setEnabled(false);

        model = new DefaultTableModel(0, 0);
        model.setColumnIdentifiers(new String[] { BILL_HEADER_ID, BILL_HEADER_NAME, BILL_HEADER_PHONE, BILL_HEADER_ROOM,
                BILL_HEADER_TOTAL_PRICE });
        reflectData(null);

        table.setAutoCreateRowSorter(true);
        table.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table.setModel(model);

        jLabel5.setText("jLabel5");
        lblTitle.setText("Bill");
        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnCancel.setText("Cancel");

        btnAdd.setText("Add");
        btnAdd.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        btnModify.setText(BILL_BTN_EDIT);
        btnModify.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        btnCancel.setText(BILL_BTN_CANCEL);
        btnCancel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        btnDelete.setText(BILL_BTN_DEL);
        btnDelete.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

        jScrollPane2.setViewportView(table);

        container = frame.getContentPane();
        GroupLayout layout = new GroupLayout(container);
        container.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup().addContainerGap().addComponent(lblTitle,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout
                                                .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                        layout.createSequentialGroup().addContainerGap()
                                                                .addComponent(jScrollPane2))
                                                .addGroup(layout.createSequentialGroup().addGap(7, 7, 7)
                                                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(btnModify, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18).addComponent(btnCancel,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE, 158,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(0, 2, Short.MAX_VALUE)))
                        .addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
                .createSequentialGroup().addGap(18, 18, 18).addComponent(lblTitle).addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(btnModify)
                        .addComponent(btnAdd).addComponent(btnDelete).addComponent(btnCancel,
                                javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap()));
        frame.pack();
        frame.setVisible(true);
    }
    
    public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		// this.setVisible(true);
	}
}

package hotel_manager;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class NotificationUI {
    public static void errorDialog(JFrame frame,String title, String content) {
        JOptionPane.showMessageDialog(frame,
                content,
                title,
                JOptionPane.ERROR_MESSAGE);
    }
    public static void warningDialog(JFrame frame,String title, String content) {
        JOptionPane.showMessageDialog(frame,
                content,
                title,
                JOptionPane.WARNING_MESSAGE);
    }
    public static void infoDialog(JFrame frame,String title, String content) {
        JOptionPane.showMessageDialog(frame,
                content,
                title,
                JOptionPane.INFORMATION_MESSAGE);
    }
}

package hotel_manager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Menu extends JFrame {
	JButton btnBooking,btnCheckIn,btnCheckOut, btnAddBillUI, btnBillUI, btnBenefit;
	public Menu(String title) {
		super(title);
		addControls();
		addEvents();
	}

	private void addEvents() {
		// TODO Auto-generated method stub
		btnBooking.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				BookingUI ui =new BookingUI("Đặt phòng");
				ui.showWin();
			}
		});
		btnCheckIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				CheckInUI ui =new CheckInUI();
				ui.showWin();
			}
		});
		btnCheckOut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				CheckOutUI ui=new CheckOutUI();
				ui.showWin();
			}
		});
		btnBenefit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				BenefitUI window = new BenefitUI();
                window.showWin();
			}
		});
		btnBillUI.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				BillUI ui = new BillUI();
				ui.showWin();
			}
		});
		
		
	}

	private void addControls() {
		// TODO Auto-generated method stub
		Container con=getContentPane();
		con.setLayout(new BorderLayout());
		// TODO Auto-generated method stub
		JPanel pnNorth =new JPanel();
		con.add(pnNorth,BorderLayout.NORTH);
		JPanel pnCenter =new JPanel();
		con.add(pnCenter,BorderLayout.CENTER);
		
		pnNorth.setLayout(new BorderLayout());
		JPanel pnTitle =new JPanel();
		pnNorth.add(pnTitle,BorderLayout.NORTH);
		
		pnTitle.setLayout(new BoxLayout(pnTitle, BoxLayout.Y_AXIS));
		JPanel pnMenuTile =new JPanel();
		JLabel lbTitile =new JLabel("Menu");
		lbTitile.setBackground(Color.BLUE);
		Font ft=new Font("arial",Font.BOLD,20);
		lbTitile.setFont(ft);
		pnMenuTile.add(lbTitile);
		pnTitle.add(pnMenuTile);
		
		pnCenter.setLayout(new BorderLayout());
		JPanel pnAction =new JPanel();
		pnCenter.add(pnAction,BorderLayout.CENTER);
		
		pnAction.setLayout(new BoxLayout(pnAction, BoxLayout.X_AXIS));
		JPanel JpBtnAdd =new JPanel();
		btnBooking=new JButton("Đặt phòng");
		JpBtnAdd.add(btnBooking);
		pnAction.add(JpBtnAdd);
		
		JPanel JpBtnDelete =new JPanel();
		btnCheckIn=new JButton("Nhận phòng");
		JpBtnDelete.add(btnCheckIn);
		pnAction.add(JpBtnDelete);
	
		JPanel JpBtnEdit =new JPanel();
		btnCheckOut=new JButton("Trả phòng");
		JpBtnEdit.add(btnCheckOut);
		pnAction.add(JpBtnEdit);
		
		JPanel JpBtnBenefit =new JPanel();
		btnBenefit=new JButton("Benefit");
		JpBtnBenefit.add(btnBenefit);
		pnAction.add(JpBtnBenefit);
		
		JPanel JpBtnAddBillU =new JPanel();
		btnBillUI=new JButton("Bill");
		JpBtnAddBillU.add(btnBillUI);
		pnAction.add(JpBtnAddBillU);
		
	}
	public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}

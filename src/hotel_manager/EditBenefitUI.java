package hotel_manager;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import hotel_interface.IBenefitService;
import hotel_model.Benefit;

import static hotel_util.Messages.*;

@SuppressWarnings("serial")
public class EditBenefitUI extends JFrame {

    private JLabel lblName, lblPrice;
    private JTextField txtName, txtPrice;
    private JButton btnFinish, btnClose;
    private IBenefitService benefitSer;
    private Integer idUpdate;

    public EditBenefitUI(IBenefitService service, Integer id) {
        initializeUI(service,id);
        initializeEvents();
    }

    private void initializeEvents() {
        btnFinish.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                EditBenefit();
            }
        });
        btnClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                closeWindow();
            }
        });
    }

    private void initializeUI(IBenefitService service, Integer id) {
        benefitSer = service;
        lblName = new JLabel();
        lblPrice = new JLabel();
        btnFinish = new JButton();
        btnClose = new JButton();
        txtName = new JTextField();
        txtPrice = new JTextField();
        idUpdate = id;
        setTitle(ADD_BENEFIT_TITLE);
        if (idUpdate != null) {
            Benefit bf = benefitSer.getBenefit(id);
            txtName.setText(bf.getBenefitName());
            txtPrice.setText(String.valueOf(bf.getBenefitPrice()));
            setTitle(UPDATE_BENEFIT_TITLE);
        }

        lblName.setText(ADD_BENEFIT_NAME_LBL);

        lblPrice.setText(ADD_BENEFIT_PRICE_LBL);

        btnFinish.setText(ADD_BENEFIT_FINISH_BT);
        
        btnClose.setText(ADD_BENEFIT_CANCEL_BT);
        

        Container pane = getContentPane();
        GroupLayout layout = new GroupLayout(pane);
        pane.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                layout.createSequentialGroup().addContainerGap()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(txtPrice).addComponent(txtName))
                                        .addContainerGap())
                        .addGroup(layout
                                .createSequentialGroup().addGap(24, 24, 24).addComponent(btnFinish)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71,
                                        Short.MAX_VALUE)
                                .addComponent(btnClose).addGap(38, 38, 38))
                        .addGroup(layout.createSequentialGroup().addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblName).addComponent(lblPrice,
                                                javax.swing.GroupLayout.PREFERRED_SIZE, 71,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup().addGap(35, 35, 35).addComponent(lblName).addGap(18, 18, 18)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                        .addComponent(lblPrice).addGap(18, 18, 18)
                        .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnFinish).addComponent(btnClose))
                        .addContainerGap()));

        pack();

        setLocation(600, 300);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    protected void EditBenefit() {
        String nameStr = txtName.getText();
        String priceStr = txtPrice.getText();
        if (nameStr.isEmpty() && priceStr.isEmpty()) {
            NotificationUI.errorDialog(this, ERROR_TITLE_INVALID_INPUT,
                    BENEFIT_NAME_HEADER + " , " + BENEFIT_PRICE_HEADER + ERROR_CONTENT_INPUTS_EMPTY);
            return;
        }
        double price = 0;
        try {
            price = Double.parseDouble(priceStr);
        } catch (Exception e) {
            NotificationUI.errorDialog(this, ERROR_TITLE_INVALID_INPUT, ADD_BENEFIT_INVALID_PRICE);
            return;
        }
        if (idUpdate == null) {
            if (benefitSer.createBenefit(nameStr, price)) {
                NotificationUI.infoDialog(this, ADD_BENEFIT_TITLE, ADD_BENEFIT_SUCCESS);
                txtName.setText("");
                txtPrice.setText("");
            } else {
                NotificationUI.warningDialog(this, ADD_BENEFIT_TITLE, ADD_BENEFIT_FAILURE);
            }
        } else {
            if (benefitSer.modifyBenefit(idUpdate,nameStr, price) > 0) {
                NotificationUI.infoDialog(this, UPDATE_BENEFIT_TITLE, UPDATE_BENEFIT_SUCCESS);
                txtName.setText("");
                txtPrice.setText("");
            } else {
                NotificationUI.warningDialog(this, UPDATE_BENEFIT_TITLE, UPDATE_BENEFIT_FAILURE);
            }
        }

    }

    protected void closeWindow() {
        this.dispose();
    }
}

package hotel_manager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import hotel_connect.CheckInService;

public class CheckInUI extends javax.swing.JFrame {
	private static final long serialVersionUID = 1L;
	
	private CheckInService checkInService;
	private Map<String, Object> bookingInfo;
	
	public CheckInUI() {
        initComponents();
        
        txtName.setEditable(false);
        txtID.setEditable(false);
        txtRoomType.setEditable(false);
        txtPrice.setEditable(false);
        
        checkInService = new CheckInService();
        bookingInfo = new HashMap<String, Object>();
    }
	
	private void reset() {
		bookingInfo = new HashMap<String, Object>();
		
		txtArr.setText("");
		txtGoTime.setText("");
		txtID.setText("");
		txtName.setText("");
		txtPhone.setText("");
		txtPrice.setText("");
		txtQuantity.setText("");
		txtRoomType.setText("");
	}

    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        lb0 = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb2 = new javax.swing.JLabel();
        lb3 = new javax.swing.JLabel();
        lb4 = new javax.swing.JLabel();
        lb5 = new javax.swing.JLabel();
        lb6 = new javax.swing.JLabel();
        lb7 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        txtName = new javax.swing.JTextField();
        txtID = new javax.swing.JTextField();
        txtRoomType = new javax.swing.JTextField();
        lb8 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        txtQuantity = new javax.swing.JTextField();
        txtArr = new javax.swing.JTextField();
        txtGoTime = new javax.swing.JTextField();
        btnCheck = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lb0.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lb0.setText("FORM CHECKIN");

        lb1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb1.setText("Số điện thoại");

        lb2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb2.setText("Họ tên");

        lb3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb3.setText("CMND");

        lb4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb4.setText("Loại phòng");

        lb5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb5.setText("Số lượng khách");

        lb6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb6.setText("Ngày đến");

        lb7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb7.setText("Ngày đi");

        lb8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb8.setText("Giá");

        btnCheck.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnCheck.setText("KIỂM TRA");
        btnCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSave.setText("XÁC NHẬN");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lb2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lb3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lb4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lb5, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lb6, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lb7, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lb1, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtQuantity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                                    .addComponent(txtRoomType, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(47, 47, 47)
                                .addComponent(lb8)
                                .addGap(35, 35, 35)
                                .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtID, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtName, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtArr, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtGoTime, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                                    .addComponent(txtPhone, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(59, 59, 59)
                                .addComponent(btnCheck, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(329, 329, 329)
                        .addComponent(btnSave))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(288, 288, 288)
                        .addComponent(lb0)))
                .addContainerGap(79, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(lb0)
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb1)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCheck))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb2)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb3)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb4)
                    .addComponent(txtRoomType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb8)
                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb5)
                    .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb6)
                    .addComponent(txtArr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb7)
                    .addComponent(txtGoTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addComponent(btnSave)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    private void btnCheckActionPerformed(java.awt.event.ActionEvent evt) {                                         
        String phoneSearch = txtPhone.getText().trim();
        
        if(!phoneSearch.isEmpty()) {
        	bookingInfo = checkInService.searchBookingInfo(phoneSearch);
        	
        	if(!bookingInfo.containsKey("booking_id")) {
        		int messageWarning = JOptionPane.WARNING_MESSAGE;
                String msg = "Không tìm thấy thông tin đặt phòng theo SĐT '" + phoneSearch + "'. Vui lòng kiểm tra lại SĐT và thử lại.";
                JOptionPane.showMessageDialog(this, msg,
                        "Tìm thông tin đặt phòng", messageWarning);
        	} else {
        		txtName.setText(bookingInfo.get("customer_name").toString());
        		txtID.setText(bookingInfo.get("identity_card").toString());
        		txtRoomType.setText(bookingInfo.get("room_type").toString());
        		txtPrice.setText(bookingInfo.get("room_rates").toString());
        	}
        }
    }                                        

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {
    	int messageWarning = JOptionPane.WARNING_MESSAGE;
    	if(!bookingInfo.containsKey("booking_id")) {
            String msg = "Vui lòng tìm kiếm thông tin đặt phòng trước khi bấm lưu và thử lại.";
            JOptionPane.showMessageDialog(this, msg,
                    "Lưu thông tin đặt phòng", messageWarning);
            return;
    	}
    	
        String quanText = txtQuantity.getText().trim();
        String inText = txtArr.getText().trim();
        String outText = txtGoTime.getText().trim();
        
        Integer quantity = getIntFromString(quanText);
        if(null == quantity || quantity < 0) {
            String msg = "Số lượng khách vừa nhập không đúng định dạng. Vui lòng nhập số lượng khách là số nguyên dương và thử lại.";
            JOptionPane.showMessageDialog(this, msg,
                    "Sai định dạng", messageWarning);
            return;
        }
        
        Date checkin = getDateFromStr(inText);
        if(null == checkin) {
            String msg = "Thời gian checkin vừa nhập không đúng định dạng yyyy-MM-dd HH:mm:ss. Vui lòng nhập thời gian checkin đúng định dạng và thử lại.";
            JOptionPane.showMessageDialog(this, msg,
                    "Sai định dạng", messageWarning);
            return;
        }
        
        Date checkout = getDateFromStr(outText);
        if(null == checkout) {
            String msg = "Thời gian checkout vừa nhập không đúng định dạng yyyy-MM-dd HH:mm:ss. Vui lòng nhập thời gian checkout đúng định dạng và thử lại.";
            JOptionPane.showMessageDialog(this, msg,
                    "Sai định dạng", messageWarning);
            return;
        }
        
        // update thông tin booking
        boolean updateBooking = checkInService.updateBookingInfo(bookingInfo.get("booking_id").toString(), checkin, checkout, quantity);
        if(!updateBooking) {
        	 String msg = "Có lỗi khi cập nhật thông tin checkin, checkout đặt phòng vào CSDL. Xin vui lòng thử lại.";
             JOptionPane.showMessageDialog(this, msg,
                     "Lỗi xảy ra", messageWarning);
             return;
        }
        
        // update thông tin phòng
        boolean updateRoom = checkInService.updateRoomStatus(bookingInfo.get("room_id").toString(), 1);
        if(!updateRoom) {
        	 String msg = "Có lỗi khi cập nhật thông tin phòng vào CSDL. Xin vui lòng thử lại.";
             JOptionPane.showMessageDialog(this, msg,
                     "Lỗi xảy ra", messageWarning);
             return;
        }
        
        String message = "Cập nhật thành công thông tin đặt phòng và thông tin phòng vào CSDL.";
        JOptionPane.showMessageDialog(this, message);
        reset();
    }                                       

    private Integer getIntFromString(String intText) {
    	try {
			return Integer.parseInt(intText);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
    }
    
    private Date getDateFromStr(String dateText) {
    	Date date = null;
    	
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        if (dateText == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);

        try {
            date = sdf.parse(dateText);
        } catch (ParseException e) {
            return null;
        }
        
        return date;
    }
	public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
    
    // Variables declaration - do not modify                     
    private javax.swing.JButton btnCheck;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel lb0;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb2;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb7;
    private javax.swing.JLabel lb8;
    private javax.swing.JTextField txtArr;
    private javax.swing.JTextField txtGoTime;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtQuantity;
    private javax.swing.JTextField txtRoomType;
    // End of variables declaration                   
}
package hotel_manager;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.github.lgooddatepicker.components.DateTimePicker;

import hotel_interface.IBillService;
import hotel_model.Benefit;
import hotel_model.Bill;
import hotel_model.Booking;
import hotel_model.Customer;
import hotel_model.Room;
import hotel_model.Staff;

import static hotel_util.ModelUtil.*;
import static hotel_util.Messages.*;
import static hotel_manager.NotificationUI.*;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class AddBillUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JLabel jLabel5;
    private JLabel lblBillID;
    private JPanel jPanel1;
    private JLabel lblDateLeave;
    private JLabel lblDateArrival;
    private JLabel lblAmountGuest;
    private JLabel lblIDCard;
    private JLabel lblPhone;
    private JLabel lblCustomerName;
    private JLabel lblRoom;
    private JLabel jLabel11;
    private JTextField txtCustomerName;
    private JTextField txtPhone;
    private JTextField txtIDCard;
    private JSpinner spGuests;
    private JTextField txtRoomType;
    private JTextField txtPrice;
    private JLabel lblRoomType;
    @SuppressWarnings("rawtypes")
    private JComboBox cboRoomType;
    private JLabel lblTotalPrice;
    private JTextField txtTotalPrice;
    private JLabel lblStaff;
    private DateTimePicker dtimepArrival;
    private DateTimePicker dtimepLeave;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;
    private JTable tableService;
    private JButton btnDelSer;
    private JButton btnAddSer;
    @SuppressWarnings("rawtypes")
    private JComboBox cboStaffName;
    private JButton btnUpdate;
    private JButton btnCancel;
    private DefaultTableModel model;
    private Bill tempBill;
    private IBillService service;
    private DefaultComboBoxModel<Room> cboModelRoom;
    private DefaultComboBoxModel<Staff> cboModelStaff;
    private static final Logger logger = Logger.getLogger(AddBillUI.class);
    private JLabel lblCreateBill;
    private DateTimePicker dpkCreateDate;
    private AbstractButton btnFindCustomer;
    private static boolean hasFindDone = false;

    public AddBillUI(IBillService service){
        initializeUI(service);
        initializeEvents();
    }

    public void initializeEvents() {
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateAndUpdateFields();
            }
        });

        btnCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                closeWindow();
            }
        });
        cboRoomType.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED && hasFindDone) {
                    Room selectedRoomType = (Room) cboModelRoom.getSelectedItem();
                    String roomType = selectedRoomType.getType().getTypeName();
                    double roomPrice = selectedRoomType.getType().getPrice();
                    tempBill.setRoomType(roomType);
                    tempBill.getBooking().setRoom(selectedRoomType);
                    tempBill.getBooking().setRoom_id(selectedRoomType.getRoom_id());
                    tempBill.setRoomPrice(roomPrice);
                    txtPrice.setText(convertPrice(roomPrice));
                    txtRoomType.setText(roomType);
                    reCalculateTotalPrice();
                }
            }
        });
        btnAddSer.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                BillAddServicesUI billAddSerUI = new BillAddServicesUI(service, convertBenefitToMap(tempBill.getBenefits()), null);
                billAddSerUI.addWindowFocusListener(new WindowFocusListener() {

                    @Override
                    public void windowLostFocus(WindowEvent arg0) {
                        tempBill.getBenefits().addAll(billAddSerUI.getNewBenefits());
                        reCalculateTotalPrice();
                        model.fireTableDataChanged();
                    }

                    @Override
                    public void windowGainedFocus(WindowEvent arg0) {
                    }
                });
            }
        });
        
        btnDelSer.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteBenefit();
            }
        });
        
        btnFindCustomer.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                hasFindDone = false;
                findCustomer();
            }
        });
        
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent event) {
                logger.info("Listen table changed");
                if (event.getType() == TableModelEvent.UPDATE) {
                    reflectData(null);
                }
            }
        });
    }
    
    protected void findCustomer() {
        String sName = txtCustomerName.getText();
        String sPhone = txtPhone.getText();
        String sIDCard = txtIDCard.getText();
        Customer findCus = null;
        if (sName.isEmpty() && sPhone.isEmpty() && sIDCard.isEmpty()) {
            errorDialog(this, ERROR_TITLE_INVALID_INPUT, "The Name , Phone and ID Card should not empty.");
            txtCustomerName.setText(tempBill.getName());
            txtPhone.setText(tempBill.getPhone());
            txtIDCard.setText(tempBill.getIdCard());
            return;
        } else {
            if (!PATTERN_PHONE_NUMBER.matcher(sPhone).matches()) {
                errorDialog(this, ERROR_TITLE_INVALID_INPUT, "The Number Phone is incorrect");
                txtPhone.setText(tempBill.getPhone());
                return;
            }
            List<Customer> customers = service.getAllCustomers();
            for (Customer customer : customers) {
                if (customer.getPhone_number().equals(sPhone) && customer.getIdentity_card().equals(sIDCard)
                        && customer.getCustomer_name().equals(sName)) {
                    findCus = customer;
                    break;
                }
            }
        }
        if (findCus != null) {
            Booking b = service.findBookingCustomer(findCus.getCustomer_id());
            if (b != null) {
                Room r = b.getRoom();
                tempBill.setName(sName);
                tempBill.setIdCard(sIDCard);
                tempBill.setPhone(sPhone);
                tempBill.setBooking(b);
                tempBill.setDateArrival(convertStringToDate(b.getDate_arrival()));
                tempBill.setDateLeave(convertStringToDate(b.getDateLeave()));
                tempBill.setRoomPrice(r.getType().getPrice());
                tempBill.setRoomType(r.getRoom_type());
                tempBill.getBooking().setRoom(r);
                tempBill.getBooking().setRoom_id(r.getRoom_id());
                tempBill.getBooking().setCustomer(b.getCustomer());
                tempBill.getBooking().setCustomer(findCus);
                tempBill.getBooking().setCustomer_id(findCus.getCustomer_id());
                tempBill.getBooking().setGuests(b.getGuests());
                tempBill.setBenefits(service.getBenefitByCustomer(findCus.getCustomer_id()));
                cboModelRoom.removeAllElements();
                for (Room room : service.getAllRooms(tempBill.getBooking().getBook_id())) {
                    cboModelRoom.addElement(room);
                }
                cboModelRoom.setSelectedItem(tempBill.getBooking().getRoom_id());
                setDateTimePicker(dtimepArrival, tempBill.getDateArrival());
                setDateTimePicker(dtimepLeave, tempBill.getDateLeave());
                spGuests.setValue(tempBill.getBooking().getGuests());
                txtRoomType.setText(r.getRoom_type());
                txtPrice.setText(convertPrice(r.getType().getPrice()));
                reCalculateTotalPrice();
                model.fireTableDataChanged();
                hasFindDone = true;
            }
            
        } else {
            errorDialog(this, ERROR_TITLE_INVALID_INPUT, "The Customer does not exist");
            return;
        }
    }

    protected void reCalculateTotalPrice() {
        updateTotalPrice(tempBill);
        txtTotalPrice.setText(convertPrice(tempBill.getTotalPrice()));
    }
    
    protected void deleteBenefit() {
        int[] rows = tableService.getSelectedRows();
        if (rows.length > 0) {
            try {
                for (int i = rows.length-1; i >= 0 ; i--) {
                    int row = rows[i];
                    Integer id = (Integer) tableService.getValueAt(row, 0);
                    String name = (String) tableService.getValueAt(row, 1);
                    List<Benefit> bes = tempBill.getBenefits();
                    Iterator<Benefit> iters = bes.iterator();
                    while (iters.hasNext()) {
                        Benefit b = iters.next();
                        if (b.getBenefitId() == id) {
                            bes.remove(b);
                            break;
                        }
                    }
                    reCalculateTotalPrice();
                    model.removeRow(row);
                    model.fireTableRowsDeleted(row, row);
                    NotificationUI.infoDialog(this, BENEFIT_DELETE, BENEFIT_DELETE_SUCCESS + " " + name);
                }
            } catch (Exception e) {
                NotificationUI.errorDialog(this, BENEFIT_DELETE, BENEFIT_DELETE_FAIL);
                logger.error(e.getMessage(), e);
            }
        } else {
            NotificationUI.warningDialog(this, BENEFIT_DELETE, BENEFIT_NOT_SELECTED_ROW);
        }
    }

    @SuppressWarnings("deprecation")
    protected void validateAndUpdateFields() {
        Integer guests = Integer.parseInt(spGuests.getValue().toString());
        LocalDate localArrivalDate = dtimepArrival.datePicker.getDate();
        LocalTime localArrivalTime = dtimepArrival.timePicker.getTime();

        Date dayArrival = new Date(localArrivalDate.getYear(), localArrivalDate.getMonthValue(),
                localArrivalDate.getDayOfMonth(), localArrivalTime.getHour(), localArrivalTime.getMinute(),
                localArrivalTime.getSecond());
        LocalDate localLeaveDate = dtimepLeave.datePicker.getDate();
        LocalTime localLeaveTime = dtimepLeave.timePicker.getTime();
        Date dayLeave = new Date(localLeaveDate.getYear(), localLeaveDate.getMonthValue(),
                localLeaveDate.getDayOfMonth(), localLeaveTime.getHour(), localLeaveTime.getMinute(),
                localLeaveTime.getSecond());
        
        if (dayArrival.equals(dayLeave)) {
            errorDialog(this, ERROR_TITLE_INVALID_INPUT, "The arrival day should not the same the leave day.");
            setDateTimePicker(dtimepArrival,tempBill.getDateArrival());
            setDateTimePicker(dtimepLeave,tempBill.getDateLeave());
            return;
        } else {
            if (dayArrival.compareTo(dayLeave) > 0) {
                errorDialog(this, ERROR_TITLE_INVALID_INPUT, "The arrival day should not after the leave day.");
                setDateTimePicker(dtimepArrival,tempBill.getDateArrival());
                setDateTimePicker(dtimepLeave,tempBill.getDateLeave());
                return;
            }
        }
        if (guests <= 0) {
            errorDialog(this, ERROR_TITLE_INVALID_INPUT, "The Amount Guest should great than 0.");
            spGuests.setValue(tempBill.getBooking().getGuests());
            return;
        }
        //Implement Update Data
        tempBill.setStaff((Staff) cboModelStaff.getSelectedItem());
        Date dArrival = convertDateTimePickerToString(dtimepArrival);
        Date dLeave = convertDateTimePickerToString(dtimepLeave);
        tempBill.setDateArrival(dArrival);
        tempBill.setDateLeave(dLeave);
        tempBill.getBooking().setDate_arrival(convertDateToString(dArrival));
        tempBill.getBooking().setDateLeave(convertDateToString(dLeave));
        tempBill.getBooking().setGuests(guests);
        Date createdDate = convertDateTimePickerToString(dpkCreateDate);
        tempBill.setIdBill(createBillID(createdDate));
        tempBill.setCreatedDate(createdDate);
        if (service.addBill(tempBill) > 0) {
            infoDialog(this, ADD_BILL_TITLE, ADD_BILL_SUCCESS);
            closeWindow();
        } else {
            errorDialog(this,ADD_BILL_TITLE,ADD_BILL_FAILURE);
        }
    }

    protected void closeWindow() {
        this.dispose();
    }
    
    @SuppressWarnings("unchecked")
    public void initializeUI(IBillService service){
        this.service = service;
        tempBill = createBill();
        
        jLabel5 = new javax.swing.JLabel();
        lblBillID = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblDateLeave = new javax.swing.JLabel();
        lblDateArrival = new javax.swing.JLabel();
        lblAmountGuest = new javax.swing.JLabel();
        lblIDCard = new javax.swing.JLabel();
        lblPhone = new javax.swing.JLabel();
        lblCustomerName = new javax.swing.JLabel();
        lblRoom = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtCustomerName = new javax.swing.JTextField();
        txtPhone = new javax.swing.JTextField();
        txtIDCard = new javax.swing.JTextField();
        spGuests = new javax.swing.JSpinner();
        spGuests.setValue(0);
        txtRoomType = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        lblRoomType = new javax.swing.JLabel();
        cboRoomType = new javax.swing.JComboBox<>();
        lblTotalPrice = new javax.swing.JLabel();
        txtTotalPrice = new javax.swing.JTextField();
        lblStaff = new javax.swing.JLabel();
        dtimepArrival = new DateTimePicker();
        dtimepLeave = new DateTimePicker();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableService = new javax.swing.JTable();
        btnDelSer = new javax.swing.JButton();
        btnAddSer = new javax.swing.JButton();
        cboStaffName = new javax.swing.JComboBox<>();
        btnFindCustomer = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblCreateBill = new javax.swing.JLabel();
        dpkCreateDate = new DateTimePicker();

        jLabel5.setText("jLabel5");

        lblBillID.setText("Create Bill");
        lblBillID.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Information"));

        lblDateLeave.setText("Date Leave :");

        lblDateArrival.setText("Date Arrival :");

        lblAmountGuest.setText("Amount Guest :");

        lblIDCard.setText("Identify Card :");

        lblPhone.setText("Phone :");

        lblCustomerName.setText("Customer Name :");

        lblRoom.setText("Room Type :");

        jLabel11.setText("Price :");

        txtRoomType.setToolTipText("");

        txtPrice.setEnabled(false);

        lblRoomType.setText("Room Type :");

        lblTotalPrice.setText("Total Price :");


        lblStaff.setText("Staff Name :");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Service"));
        
        jScrollPane1.setViewportView(tableService);

        btnDelSer.setText("Delete");

        btnAddSer.setText("Add");

        btnUpdate.setText("Add");

        btnCancel.setText("Cancel");

        lblCreateBill.setText("Create Date :");

        dpkCreateDate.setEnabled(false);
        
        btnFindCustomer.setText("Find Customer");
        
        Font fontTextDisable = new Font("Tahoma", 2, 11);
        txtRoomType.setFont(fontTextDisable); // NOI18N
        txtRoomType.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtRoomType.setEnabled(false);
        txtPrice.setFont(fontTextDisable);
        txtPrice.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtPrice.setEnabled(false);
        txtTotalPrice.setFont(fontTextDisable);
        txtTotalPrice.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtTotalPrice.setEnabled(false);

        setDateTimePicker(dpkCreateDate, new Date());
        cboModelRoom = new DefaultComboBoxModel<Room>();
        cboRoomType.setModel(cboModelRoom);
        cboModelStaff = new DefaultComboBoxModel<Staff>(convertToVecStaff(service.getAllStaffs()));
        cboStaffName.setModel(cboModelStaff);
        model = new DefaultTableModel(0, 0);
        model.setColumnIdentifiers(HEADERS_BENEFIT);
        tableService.setModel(model);
        reflectData(null);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAddSer, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDelSer, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddSer)
                    .addComponent(btnDelSer))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblTotalPrice)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotalPrice))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCustomerName)
                        .addGap(33, 33, 33)
                        .addComponent(txtCustomerName))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblDateLeave)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPhone)
                                .addGap(79, 79, 79)
                                .addComponent(txtPhone))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblIDCard)
                                .addGap(45, 45, 45)
                                .addComponent(txtIDCard))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblDateArrival)
                                    .addComponent(lblRoom)
                                    .addComponent(lblStaff))
                                .addGap(52, 52, 52)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(dtimepArrival, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dtimepLeave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboStaffName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboRoomType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblAmountGuest)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(spGuests))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblRoomType)
                                    .addComponent(jLabel11))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPrice)
                                    .addComponent(txtRoomType)
                                    .addComponent(btnFindCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCustomerName)
                    .addComponent(txtCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPhone)
                            .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblIDCard)
                            .addComponent(txtIDCard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnFindCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblRoom)
                    .addComponent(lblRoomType)
                    .addComponent(txtRoomType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRoomType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDateArrival)
                    .addComponent(dtimepArrival, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDateLeave)
                    .addComponent(dtimepLeave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAmountGuest)
                    .addComponent(spGuests, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStaff)
                    .addComponent(cboStaffName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotalPrice))
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblBillID, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCreateBill)
                        .addGap(35, 35, 35)
                        .addComponent(dpkCreateDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBillID)
                    .addComponent(lblCreateBill)
                    .addComponent(dpkCreateDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        
        pack();
        setLocation(600, 300);
        setVisible(true);
        setSize(600, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void reflectData(Integer isUpdateRow) {
        List<Integer> ids = new ArrayList<Integer>();
        Enumeration enums = model.getDataVector().elements();
        while (enums.hasMoreElements()) {
            Vector<Object> vector = (Vector<Object>) enums.nextElement();
            ids.add((Integer) vector.get(0));
        }
        if (isUpdateRow != null) {
        } else {
            if (tempBill != null) {
                for (Benefit bf : tempBill.getBenefits()) {
                    Integer id = bf.getBenefitId();
                    if (!ids.contains(id)) {
                        Vector<Object> vector = new Vector<Object>();
                        vector.add(id);
                        vector.add(bf.getBenefitName());
                        vector.add(convertPrice(bf.getBenefitPrice()));
                        model.addRow(vector);
                    }
                }
            }
        }
    }
    public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}

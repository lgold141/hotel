package hotel_manager;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class SearchUI extends JFrame {
	JTextField txtSearch;
	JButton btnSearch;
	DefaultTableModel dtmBooking;
	JTable tblBooking;
	public SearchUI(String title) {
		super(title);
		addControls();
		addEvents();
	}

	private void addEvents() {
		// TODO Auto-generated method stub
		
	}

	private void addControls() {
		// TODO Auto-generated method stub
		Container con =getContentPane();
		con.setLayout(new BorderLayout());
		JPanel pnNorth =new JPanel();
		pnNorth.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lbWord =new JLabel("Từ cần tìm");
		txtSearch = new JTextField(20);
		btnSearch=new JButton("Tìm kiếm");
		pnNorth.add(lbWord);
		pnNorth.add(txtSearch);
		pnNorth.add(btnSearch);
		con.add(pnNorth,BorderLayout.NORTH);
		
		JPanel pnCenter =new JPanel();
		pnCenter.setLayout(new BorderLayout());
		dtmBooking=new  DefaultTableModel();
		dtmBooking.addColumn("Mã đặt phòng");
		dtmBooking.addColumn("Mã khách hàng");
		dtmBooking.addColumn("Mã phòng");
		dtmBooking.addColumn("Ngày đến");
		dtmBooking.addColumn("Ngày nhận phòng");
		dtmBooking.addColumn("Ngày trả phòng");
		tblBooking =new JTable(dtmBooking);
		JScrollPane scBooking =new JScrollPane(tblBooking,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnCenter.add(scBooking,BorderLayout.CENTER);
		con.add(pnCenter,BorderLayout.CENTER);
	}
	public void showWin() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}

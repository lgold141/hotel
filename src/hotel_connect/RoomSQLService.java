package hotel_connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import static hotel_util.ModelUtil.*;
import hotel_model.Room;


public class RoomSQLService extends MysqlService {

    private static final String UPDATE_ROOM = "update room set type_id=? where room_id=?";
    
    private static final String GET_ALL_ROOM = "select r.*,rt.* from room r inner join room_type rt on r.type_id=rt.type_id";
    private static final String GET_ALL_ROOM_ID = "select room_id from room";

    public static final Logger logger = Logger.getLogger(RoomSQLService.class);

    public List<Room> getAllRooms() {
        List<Room> types = new ArrayList<Room>();
        try (Connection con = getConnection();
             Statement statement = con.createStatement();
             ResultSet rs = statement.executeQuery(GET_ALL_ROOM);){
            while (rs.next()) {
                types.add(convertToRoom(rs));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return types;
    }
    public Vector getListRoomId(){
    	Vector CB_Room = new Vector();
    	try (Connection con = getConnection();
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(GET_ALL_ROOM_ID);){
               while (rs.next()) {
            	   CB_Room.add(rs.getString("Room_Id"));
               }
           } catch (Exception e) {
               logger.error(e.getMessage(), e);
           }
		return CB_Room;
    }

    public int updateRoom(String typeID, String roomID) {
        try (Connection con = getConnection(); PreparedStatement statement = con.prepareStatement(UPDATE_ROOM);) {
            logger.info("update room");
            statement.setString(1, typeID);
            statement.setString(2, roomID);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
}

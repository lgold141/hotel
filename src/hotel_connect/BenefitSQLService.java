package hotel_connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static hotel_util.ModelUtil.*;

import org.apache.log4j.Logger;

import hotel_model.Benefit;

public class BenefitSQLService extends MysqlService {

    private static final String GET_ALL_BENEFITS = "select * from Benefit";
    private static final String ADD_BENEFITS = "INSERT INTO benefit (benefit_name, benefit_price)" + "VALUES (?,?)";
    private static final String FIND_BENEFIT_BY_NAME = "select * from benefit where benefit_name=?";
    private static final String FIND_BENEFIT_BY_ID = "select * from benefit where benefit_id=?";
    private static final String DEL_BENEFIT = "delete from benefit where benefit_id=?";
    private static final String UPDATE_BENEFIT = "update benefit set benefit_name=? , benefit_price=? where benefit_id=?";
    public static final Logger logger = Logger.getLogger(BenefitSQLService.class);

    @SuppressWarnings("finally")
    public List<Benefit> getAllBenefits() {
        List<Benefit> benefits = new ArrayList<Benefit>();
        logger.info("Start get all benefits");
        try ( Connection con = getConnection();
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(GET_ALL_BENEFITS);) {
            while (rs.next()) {
                benefits.add(convertToBenefit(rs));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info("Finish");
            return benefits;
        }
    }

    public boolean addBenefit(String name, double price) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(ADD_BENEFITS);) {
            logger.info("add benefit");
            statement.setString(1, name);
            statement.setDouble(2, price);
            statement.execute();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public int updateBenefit(Integer id, String name, double price) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(UPDATE_BENEFIT);) {
            logger.info("update benefit");
            statement.setString(1, name);
            statement.setDouble(2, price);
            statement.setInt(3, id);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }

    public Benefit findBenefitByName(String name) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(FIND_BENEFIT_BY_NAME);) {
            logger.info("find benefit by name");
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return convertToBenefit(rs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public Benefit findBenefitByID(Integer id) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(FIND_BENEFIT_BY_ID);) {
            logger.info("find benefit by id");
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return convertToBenefit(rs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public int deleteBenefitByName(Integer id) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(DEL_BENEFIT);) {
            logger.info("delete benefit");
            statement.setInt(1, id);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return 0;
    }
}

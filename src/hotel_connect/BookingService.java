package hotel_connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import hotel_model.Booking;
import hotel_util.ModelUtil;

public class BookingService extends MysqlService {
    
    private static final String GET_BOOKING = "select * from booking";
    private static final String ADD_BOOKING = "INSERT INTO booking (book_id, customer_id, room_id, date_arrival, date_leave, check_in, check_out, guests) VALUES (?,?,?,?,?,?,?,?)";
    private static final String UPDATE_BOOKING = "update booking set date_arrival=? , date_leave=? , guests=?,room_id=? where book_id=?";

    public static final String UPDATE_BOOKING_2 = "update booking set date_arrival='%s' , date_leave='%s' , guests=%d ,room_id='%s' where book_id='%s'";
    
    private static final String FIND_BOOKING_BY_CUSTOMER = "select bk.*,r.*,rt.* from customer c INNER JOIN booking bk ON"
            +" bk.customer_id=c.customer_id LEFT JOIN room r ON r.room_id=bk.room_id LEFT JOIN room_type rt ON"
            +" rt.type_id=r.type_id where c.customer_id=? and bk.book_id NOT IN (select bill.book_id FROM bill)";
    
    public static final Logger logger = Logger.getLogger(BookingService.class);

    public Booking findBookingByCustomerID(String cusID) {
        try ( Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(FIND_BOOKING_BY_CUSTOMER);) {
              logger.info("find booking");
              statement.setString(1, cusID);
             ResultSet rs = statement.executeQuery();
             if (rs.next()) {
                 return ModelUtil.convertToBooking(rs);
             }
          } catch (Exception e) {
              logger.error(e.getMessage(), e);
          }
        return null;
    }

    public List<Booking> getAllBooking() {
        List<Booking> dsBooking = new ArrayList<Booking>();
        try ( Connection con = getConnection();
                PreparedStatement pr = con.prepareStatement(GET_BOOKING); 
                ResultSet rs = pr.executeQuery();) {
            logger.info("get all booking");
            while (rs.next()) {
                Booking bk = new Booking();
                bk.setBook_id(rs.getString(1));
                bk.setCustomer_id(rs.getString(2));
                bk.setRoom_id(rs.getString(3));
                bk.setDate_arrival(rs.getString(4));
                bk.setDate_leave(rs.getString(5));
                bk.setGuests(rs.getInt(8));
                dsBooking.add(bk);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return dsBooking;
    }
    public boolean addBooking(String bookingId, String customerId,String roomId, String dateArival, String dateLeave,String checkIn,String checkOut, String guests) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(ADD_BOOKING);) {
            logger.info("add booking");
            statement.setString(1, bookingId);
            statement.setString(2, customerId);
            statement.setString(3, roomId);
            statement.setString(4, dateArival);
            statement.setString(5, dateLeave);
            statement.setString(6, checkIn);
            statement.setString(7, checkOut);
            statement.setString(8, guests);
            statement.execute();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }
    public int updateBooking(String dateArrival, String dateLeave, int guests, String bookID, String roomID) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(UPDATE_BOOKING);) {
            logger.info("update booking");
            statement.setString(1, dateArrival);
            statement.setString(2, dateLeave);
            statement.setInt(3, guests);
            statement.setString(4, roomID);
            statement.setString(5, bookID);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
//	public void addBooking(String a, String a1, String a2, String a3, String a4, String a5){
//		try {
//			String sql="INSERT INTO booking VALUES ('"+a+"','"+a1+"','"+a2+"','"+a3+"','"+a4+"','"+a5+"')";
//			statement =(Statement) conn.createStatement();
//			statement.execute();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//	}
}

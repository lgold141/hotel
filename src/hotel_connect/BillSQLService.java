package hotel_connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import static hotel_util.ModelUtil.*;

import org.apache.log4j.Logger;

import hotel_model.Bill;

public class BillSQLService extends MysqlService {
    private static final String GET_ALL_BILLS = "select bl.*, st.staff_name, bk.customer_id,bk.room_id,bk.date_arrival,bk.check_in,bk.check_out,bk.guests, "
            + "ct.customer_name,ct.identity_card,ct.phone_number, rm.type_id,rm.room_rate, rt.type_name,rt.price,bk.date_leave "
            + "from bill bl inner join booking bk on bl.book_id=bk.book_id inner join staff st on st.staff_id=bl.staff_id "
            + "left join room rm on rm.room_id=bk.room_id left join customer ct on ct.customer_id=bk.customer_id "
            + "left join room_type rt on rt.type_id=rm.type_id ";
    private static final String ADD_BILL = "INSERT INTO bill (bill_id, book_id,bill_date,staff_id)"
            + "VALUES (?,?,?,?)";
    public static final String ADD_BILL_2 = "INSERT INTO bill (bill_id, book_id,bill_date,staff_id)"
            + "VALUES ('%s','%s','%s','%s')";
    private static final String FIND_BILL_BY_CUSTOMER = "select * from bill where book_id=?";
    private static final String FIND_BILL_BY_ID = "select bl.*, st.staff_name, bk.customer_id,bk.room_id,bk.date_arrival,bk.check_in,bk.check_out,bk.guests, "
            + "ct.customer_name,ct.identity_card,ct.phone_number, rm.type_id,rm.room_rate, rt.type_name,rt.price,bk.date_leave "
            + "from bill bl inner join booking bk on bl.book_id=bk.book_id inner join staff st on st.staff_id=bl.staff_id "
            + "left join room rm on rm.room_id=bk.room_id left join customer ct on ct.customer_id=bk.customer_id "
            + "left join room_type rt on rt.type_id=rm.type_id where bill_id=?";
    private static final String DEL_BILL = "delete from bill where bill_id=?";
    private static final String UPDATE_BILL = "update bill set staff_id=? where bill_id=?";
    public static final String UPDATE_BILL_2 = "update bill set staff_id='%s' where bill_id='%s'";

    public static final Logger logger = Logger.getLogger(BenefitSQLService.class);

    @SuppressWarnings("finally")
    public List<Bill> getAllBill() {
        List<Bill> bill = new ArrayList<Bill>();
        try (Connection con = getConnection();
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(GET_ALL_BILLS);) {
            logger.info("Start get all bill");
            while (rs.next()) {
                bill.add(convertToBill(rs));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info("Finish");
            return bill;
        }
    }

    public boolean addBenefit(String billID, String bookID, String staffID, String date) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(ADD_BILL);) {
            logger.info("add bill");
            statement.setString(1, billID);
            statement.setString(2, bookID);
            statement.setString(3, date);
            statement.setString(4, staffID);
            statement.execute();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public int updateBill(String staffID, String billID) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(UPDATE_BILL);) {
            logger.info("update Bill");
            statement.setString(1, staffID);
            statement.setString(2, billID);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
    
    public Bill findBillByCustomer(String bookID) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(FIND_BILL_BY_CUSTOMER);) {
            logger.info("find bill by bookid");
            statement.setString(1, bookID);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return convertToBill(rs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public Bill findBillByID(String id) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(FIND_BILL_BY_ID);) {
            logger.info("find bill by id");
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return convertToBill(rs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public int deleteBillByID(String id) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(DEL_BILL);) {
            logger.info("delete bill");
            statement.setString(1, id);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return 0;
    }
}

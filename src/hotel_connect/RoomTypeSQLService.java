package hotel_connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import static hotel_util.ModelUtil.*;

import hotel_model.RoomType;

public class RoomTypeSQLService extends MysqlService{
    
    private static final String GET_ALL_ROOMTYPE = "select * from room_type";
    private static final String FIND_ROOMTYPE_BY_NAME = "select * from room_type where type_name=?";
    public static final Logger logger = Logger.getLogger(RoomTypeSQLService.class);


    public List<RoomType> getAllRoomTypes() {
        List<RoomType> types = new ArrayList<RoomType>();
        try (Connection con = getConnection();
             Statement statement = con.createStatement();
             ResultSet rs = statement.executeQuery(GET_ALL_ROOMTYPE);){
            while (rs.next()) {
                types.add(convertToRoomType(rs));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return types;
    }
    
    public RoomType findRoomTypeByName(String name) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(FIND_ROOMTYPE_BY_NAME);) {
            logger.info("find room_type by name");
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return convertToRoomType(rs);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}

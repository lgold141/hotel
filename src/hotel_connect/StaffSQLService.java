package hotel_connect;

import static hotel_util.ModelUtil.convertToStaff;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import hotel_model.Staff;

public class StaffSQLService extends MysqlService {
    
    private static final String GET_ALL_STAFF = "select * from staff";
    public static final Logger logger = Logger.getLogger(RoomTypeSQLService.class);


    public List<Staff> getAllStaffs() {
        List<Staff> types = new ArrayList<Staff>();
        try (Connection con = getConnection();
                Statement statement = con.createStatement();
             ResultSet rs = statement.executeQuery(GET_ALL_STAFF);){
            while (rs.next()) {
                types.add(convertToStaff(rs));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return types;
    }
}

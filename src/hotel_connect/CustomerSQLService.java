package hotel_connect;

import static hotel_util.ModelUtil.convertToCustomer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import hotel_model.Customer;

public class CustomerSQLService extends MysqlService {
    private static final String GET_ALL_CUSTOMERS = "select * from customer";
    private static final String GET_ALL_CUSTOMERS_ID = "select customer_id from customer";
    
    public static final String UPDATE_CUSTOMER = "update customer set customer_name=?, identity_card=?, phone_number=? where customer_id=?";
    public static final String UPDATE_CUSTOMER_2 = "update customer set customer_name='%s', identity_card=%s, phone_number=%s where customer_id='%s'";
    
    public static final Logger logger = Logger.getLogger(CustomerSQLService.class);


    public List<Customer> getAllCustomers() {
        List<Customer> types = new ArrayList<Customer>();
        try (Connection con = getConnection();
             Statement statement = con.createStatement();
             ResultSet rs = statement.executeQuery(GET_ALL_CUSTOMERS);){
            logger.info("get all customers");
            while (rs.next()) {
                types.add(convertToCustomer(rs));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return types;
    }
    public Vector getCustomerID() {
    	Vector CB_Customer =new Vector();
    	try (Connection con = getConnection();
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(GET_ALL_CUSTOMERS);){
               logger.info("get all customers");
               while (rs.next()) {
            	   CB_Customer.add(rs.getString("Customer_Id"));               }
           } catch (Exception e) {
               logger.error(e.getMessage(), e);
           }
    	return CB_Customer;
    }
    
    public int updateCustomer(String cusName, String cusIDCard, String cusPhone, String cusID) {
        try (Connection con = getConnection(); PreparedStatement statement = con.prepareStatement(UPDATE_CUSTOMER);) {
            logger.info("update customer");
            statement.setString(1, cusName);
            statement.setString(2, cusIDCard);
            statement.setString(3, cusPhone);
            statement.setString(4, cusID);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }
}

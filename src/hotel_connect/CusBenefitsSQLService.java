package hotel_connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import hotel_model.Benefit;
import hotel_util.ModelUtil;

public class CusBenefitsSQLService extends MysqlService {
    private static final String GET_BENEFITS_OF_CUSTOMER = "select ct.customer_id,b.benefit_id,cb.date,ct.customer_name,b.benefit_name,b.benefit_price "
            + "from cus_benefit cb inner join benefit b on cb.benefit_id=b.benefit_id left join "
            + "customer ct on ct.customer_id=cb.customer_id where cb.customer_id=?";
    
    private static final String ADD_BENEFITS_OF_CUSTOMER = "INSERT INTO cus_benefit (customer_id, benefit_id,date)" + "VALUES (?,?,?)";
    public static final String ADD_BENEFITS_OF_CUSTOMER_2 = "INSERT INTO cus_benefit (customer_id, benefit_id,date)" + "VALUES ('%s','%s','%s')";
    private static final String DEL_BENEFITS_OF_CUSTOMER = "delete from cus_benefit where customer_benefit_id=?";
    public static final String DEL_BENEFITS_OF_CUSTOMER_BY_CUS = "delete from cus_benefit where customer_id='%s'";
    

    public static final Logger logger = Logger.getLogger(CusBenefitsSQLService.class);

    
    public List<Benefit> getBenefitsOfCustomer(String customerID){
        List<Benefit> cusBes = new ArrayList<Benefit>();
        try(Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(GET_BENEFITS_OF_CUSTOMER);){
             logger.info("get all cus_benefits");
             statement.setString(1, customerID);
             ResultSet res = statement.executeQuery();
             while (res.next()) {
                Benefit be = new Benefit();
                be.setBenefitId(res.getInt(2));
                be.setBenefitName(res.getString(5));
                be.setBenefitPrice(res.getDouble(6));
                be.setCusBenefitCreatedDay(ModelUtil.convertStringToDate(res.getString(3)));
                cusBes.add(be);
            }
            return cusBes;
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return cusBes;
    }
    
    public boolean addBenefitsOfCustomer(String customerID, int benefitID, String date) {
        try ( Connection con = getConnection();
              PreparedStatement statement = con.prepareStatement(ADD_BENEFITS_OF_CUSTOMER);) {
            logger.info("add cus_benefit");
            statement.setString(1, customerID);
            statement.setInt(2, benefitID);
            statement.setString(3, date);
            statement.execute();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }
    
    public int deleteBenefitsOfCustomer(Integer id) {
        try (Connection con = getConnection();
                PreparedStatement statement = con.prepareStatement(DEL_BENEFITS_OF_CUSTOMER);) {
            logger.info("delete cus_benefit");
            statement.setInt(1, id);
            return statement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return 0;
    }
}

package hotel_connect;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import hotel_model.CheckOut;

public class CheckOutService extends MysqlService {
	public boolean saveCheckOutData(CheckOut checkOut) {
        String sql = "INSERT INTO checkout(book_id, room_price, services_price, update_time) VALUES(?, ?, ?, ?);";

		try (PreparedStatement pr = getConnection().prepareStatement(sql);){
			pr.setString(1, checkOut.getBookingId());
			pr.setLong(2, checkOut.getRoomPrice());
			pr.setLong(3, checkOut.getServicesPrice());
			pr.setString(4, checkOut.getUpdateTime());

			pr.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();

			return false;
		}
	}
	
	public Map<String, Object> searchBookingInfo(String phoneNumber) {
		Map<String, Object> bookingInfo = new HashMap<String, Object>();
		String sql = "SELECT * FROM booking B INNER JOIN customer C ON B.customer_id = C.customer_id INNER JOIN room R ON B.room_id = r.room_id "
                + "WHERE C.phone_number = ? AND B.book_id NOT IN (SELECT book_id FROM checkout);";
		try (PreparedStatement pr = getConnection().prepareStatement(sql);){
			pr.setString(1, phoneNumber);
			ResultSet rs = pr.executeQuery();
			if (rs.next()) {
				bookingInfo.put("booking_id", rs.getString("book_id"));
				bookingInfo.put("customer_id", rs.getString("customer_id"));
				bookingInfo.put("room_id", rs.getString("room_id"));
				bookingInfo.put("date_arrival", rs.getString("date_arrival"));

				bookingInfo.put("customer_name", rs.getString("customer_name"));
				bookingInfo.put("identity_card", rs.getString("identity_card"));
				bookingInfo.put("room_type", rs.getString("room_type"));
				bookingInfo.put("room_rates", rs.getInt("room_rates"));
				bookingInfo.put("slot", rs.getInt("slot"));
				
				bookingInfo.put("check_in", rs.getString("check_in"));
				bookingInfo.put("check_out", rs.getString("check_out"));

				return bookingInfo;
			}

		} catch (SQLException e) {
			e.printStackTrace();

			return new HashMap<>();
		}

		return bookingInfo;
	}
	
	public boolean updateRoomStatus(String roomId, int status) {
        String sql = "UPDATE room SET room_status = ? WHERE room_id = ?;";
		try(PreparedStatement pr = getConnection().prepareStatement(sql);){
			pr.setInt(1, status);
			pr.setString(2, roomId);

			pr.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();

			return false;
		}
	}
}
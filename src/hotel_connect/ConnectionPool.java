package hotel_connect;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ConnectionPool {

    private static final String JDBC_DB_URL = "jdbc:mysql://localhost/hotel?useUnicode=true&characterEncoding=utf-8&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "";
    private static final int MAX_CONNECTION_POOL = 10;
    private static final long MAX_WAIT_BORROWING = 30000;
    @SuppressWarnings("rawtypes")
    private GenericObjectPool pool;
    
    @SuppressWarnings({ "rawtypes", "unused" })
    public ConnectionPool() {
     // Creates an Instance of GenericObjectPool That Holds Our Pool of Connections Object!
        pool = new GenericObjectPool();
        pool.setMaxActive(MAX_CONNECTION_POOL);
        pool.setMaxWait(MAX_WAIT_BORROWING);
        pool.setTestOnBorrow(true);
        // Creates a ConnectionFactory Object Which Will Be Use by the Pool to Create the Connection Object!
        ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);
 
        // Creates a PoolableConnectionFactory That Will Wraps the Connection Object Created by the ConnectionFactory to Add Object Pooling Functionality!
        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, pool, null, null, false, true);        
    }
    
    public DataSource getDataSource(){
        return new PoolingDataSource(pool);
    }
 
    @SuppressWarnings("rawtypes")
    public GenericObjectPool getConnectionPool() {
        return pool;
    }
}

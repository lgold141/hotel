package hotel_connect;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;
import static hotel_util.Messages.*;
import org.apache.log4j.Logger;

public class MysqlService {
    public static final Logger logger = Logger.getLogger(MysqlService.class);
    private static DataSource data;
    private static ConnectionPool pool = null;

    public MysqlService() {
        try {
            if (pool == null) {
                pool = new ConnectionPool();
                data = pool.getDataSource();
            }
            logger.info(printStatusPool());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private String printStatusPool() {
        return "Max.: " + pool.getConnectionPool().getMaxActive() + "; Active: "
                + pool.getConnectionPool().getNumActive() + "; ExhaustedAction: "
                + pool.getConnectionPool().getWhenExhaustedAction();
    }
    
    
    public int executeMultiSQL(List<String> sqls) {
        Connection con = null;
        Statement statement = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            logger.info("update " + sqls);
            statement = con.createStatement();
            for (String sql : sqls) {
                statement.addBatch(sql);
            }
            statement.executeBatch();
            con.commit();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            try {
                con.rollback();
            } catch (SQLException e1) {
                logger.error(e.getMessage(), e);
            }
            return 0;
        } finally {
            try {
                statement.close();
                con.close();
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return 1;
    }


    public Connection getConnection() {
        try {
            logger.info(printStatusPool());
            if (pool.getConnectionPool().getMaxActive() == pool.getConnectionPool().getNumActive()) {
                throw new SQLException(CONNECTION_POOL_IS_FULL);
            }
            return data.getConnection();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}

package hotel_util;

import java.time.ZoneId;
import java.util.regex.Pattern;

public class Messages {

    public static final String CONNECTION_POOL_IS_FULL = "The connection pool is full!";

    public static final int BENEFIT_COL_ID = 1;
    public static final int BENEFIT_COL_NAME = 2;
    public static final int BENEFIT_COL_PRICE = 3;

    public static final String ADD_BENEFIT_NAME_LBL = "Name :";
    public static final String ADD_BENEFIT_PRICE_LBL = "Price : VND";
    public static final String ADD_BENEFIT_FINISH_BT = "Finish";
    public static final String ADD_BENEFIT_CANCEL_BT = "Cancel";
    public static final String ADD_BENEFIT_TITLE = "Add Benefit Service";
    public static final String ADD_BENEFIT_INVALID_PRICE = "Please input price is Number type";
    public static final String ADD_BENEFIT_FAILURE = "Failure when added new Benefit";
    public static final String ADD_BENEFIT_SUCCESS = "Created new Benefit";

    public static final String UPDATE_BENEFIT_TITLE = "Update Benefit Service";
    public static final String UPDATE_BENEFIT_INVALID_PRICE = "Please input price is Number type";
    public static final String UPDATE_BENEFIT_FAILURE = "Failure when updated new Benefit";
    public static final String UPDATE_BENEFIT_SUCCESS = "Updated Benefit";

    public static final String BENEFIT_ID_HEADER = "ID";
    public static final String BENEFIT_NAME_HEADER = "Name";
    public static final String BENEFIT_PRICE_HEADER = "Price";
    public static final String BENEFIT_TITLE = "Benefit Service";
    public static final String BENEFIT_DELETE = "Benefit Delete";
    public static final String BENEFIT_NOT_SELECTED_ROW = "Please select Benefit before edit !";
    public static final String BENEFIT_SELECTED_SINGLE_ROW = "Should select one Benefit!";
    public static final String BENEFIT_DELETE_FAIL = "Failure when deleted";
    public static final String BENEFIT_DELETE_SUCCESS = "Deleted Benefit";
    public static final String BENEFIT_MODIFY = "Benefit Update";

    public static final String BILL_NOT_SELECTED_ROW = "Please select Bill before edit !";
    public static final String BILL_DELETE = "Bill Delete";
    public static final String BILL_SELECTED_SINGLE_ROW = "Should select one Bill!";
    public static final String BILL_DELETE_SUCCESS = "Deleted Bill";

    public static final String ADD_BILL_TITLE = "Add Bill Service";
    public static final String ADD_BILL_INVALID_PRICE = "Please input price is Number type";
    public static final String ADD_BILL_FAILURE = "Failure when added Bill";
    public static final String ADD_BILL_SUCCESS = "Added Bill";
    
    public static final String UPDATE_BILL_TITLE = "Update Bill Service";
    public static final String UPDATE_BILL_INVALID_PRICE = "Please input price is Number type";
    public static final String UPDATE_BILL_FAILURE = "Failure when updated Bill";
    public static final String UPDATE_BILL_SUCCESS = "Updated Bill";

    public static final String UPDATE_ADD_SER_BILL_TITLE = "Add Service To Bill";

    public static final int BILL_COL_ID = 1;
    public static final int BILL_COL_BOOKINGID = 2;
    public static final int BILL_COL_BILL_DATE = 3;
    public static final int BILL_COL_STAFFID = 4;

    public static final int ROOM_COL_ID = 1;
    public static final int ROOM_COL_TYPE = 2;
    public static final int ROOM_COL_RATE = 3;

    public static final int ROOM_TYPE_COL_ID = 1;
    public static final int ROOM_TYPE_COL_NAME = 2;
    public static final int ROOM_TYPE_COL_PRICE = 3;

    public static final int CUS_COL_ID = 1;
    public static final int CUS_COL_CUSTOMER = 2;
    public static final int CUS_COL_BIRTHDAY = 3;
    public static final int CUS_COL_GENDER = 4;
    public static final int CUS_COL_IDCARD = 5;
    public static final int CUS_COL_PHONE = 6;
    public static final int CUS_COL_NATIONALITY = 7;

    public static final int STAFF_COL_ID = 1;
    public static final int STAFF_COL_NAME = 2;
    public static final int STAFF_COL_PASS = 3;
    public static final int STAFF_COL_BIRTHDAY = 4;
    public static final int STAFF_COL_IDCARD = 5;
    public static final int STAFF_COL_GENDER = 6;
    public static final int STAFF_COL_ADDRESS = 7;

    public static final int BOOKING_COL_ID = 1;
    public static final int BOOKING_COL_CUSTOMER = 2;
    public static final int BOOKING_COL_ROOM = 3;
    public static final int BOOKING_COL_DATE_ARRIVAL = 4;
    public static final int BOOKING_COL_CHECKIN = 5;
    public static final int BOOKING_COL_CHECKOUT = 6;
    public static final int BOOKING_COL_GUEST = 7;

    public static final String BILL_ID = "BILL";
    public static final String BILL_TITLE = "Bill";
    public static final String BILL_BTN_EDIT = "Edit";
    public static final String BILL_BTN_DEL = "Delete";
    public static final String BILL_BTN_CANCEL = "Cancel";
    public static final String BILL_HEADER_ID = "ID";
    public static final String BILL_HEADER_NAME = "Customer Name";
    public static final String BILL_HEADER_PHONE = "Phone";
    public static final String BILL_HEADER_ROOM = "Room Type";
    public static final String BILL_HEADER_TOTAL_PRICE = "Total Price";

    public static final String EDIT_BILL_UPDATE_BTN = "Update";
    public static final String EDIT_BILL_CANCEL_BTN = "Cancel";
    public static final String EDIT_BILL_INFOR = "Information";
    public static final String EDIT_BILL_LBL_NAME = "Name :";
    public static final String EDIT_BILL_LBL_PHONE = "Phone :";
    public static final String EDIT_BILL_LBL_IDCARD = "Identify Card :";
    public static final String EDIT_BILL_LBL_ROOM = "Room :";
    public static final String EDIT_BILL_LBL_AMOUNT_GUEST = "Amount Guest :";
    public static final String EDIT_BILL_LBL_DATE_ARRIVAL = "Date Arrival :";
    public static final String EDIT_BILL_LBL_DATE_LEAVE = "Date Leave :";
    public static final String EDIT_BILL_LBL_ROOM_TYPE = "Room Type :";
    public static final String EDIT_BILL_LBL_ROOM_PRICE = "Room Price :";
    public static final String EDIT_BILL_LBL_TOTAL_PRICE = "Total Price :";
    public static final String EDIT_BILL_LBL_SERVICES = "Services :";
    public static final String EDIT_BILL_LBL_STAFF = "Staff Name :";
    public static final String EDIT_BILL_LBL_EDIT = "EDIT BILL";

    public static final String ERROR_TITLE_INVALID_INPUT = "Invalid Input";
    public static final String ERROR_CONTENT_INPUTS_EMPTY = "%s should not empty !";
    public static final String PATTERN_DATE = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTERN_ONLY_DAY = "yyyy-MM-dd";
    public static final String GENDER_FEMALE = "Female";
    public static final String GENDER_MALE = "Male";
    public static Pattern PATTERN_PHONE_NUMBER = Pattern
            .compile("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}");
    public static String[] HEADERS_BENEFIT = { BENEFIT_ID_HEADER, BENEFIT_NAME_HEADER, BENEFIT_PRICE_HEADER };

    public static ZoneId ZONE_SYSTEM = ZoneId.systemDefault();
}

package hotel_util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.github.lgooddatepicker.components.DateTimePicker;

import hotel_model.Benefit;
import hotel_model.Bill;
import hotel_model.Booking;
import hotel_model.Customer;
import hotel_model.Room;
import hotel_model.RoomType;
import hotel_model.Staff;

import static hotel_util.Messages.*;

public class ModelUtil {
    public static final Logger logger = Logger.getLogger(ModelUtil.class);

    public static Staff convertToStaff(ResultSet rs) throws SQLException {
        Staff st = new Staff();
        st.setId(rs.getString(STAFF_COL_ID));
        st.setName(rs.getString(STAFF_COL_NAME));
        st.setPass(rs.getString(STAFF_COL_PASS));
        st.setBirthDay(convertStringToDate(rs.getString(STAFF_COL_BIRTHDAY),true));
        st.setIdCard(rs.getInt(STAFF_COL_IDCARD));
        st.setGender(rs.getInt(STAFF_COL_GENDER));
        st.setAddress(rs.getString(STAFF_COL_ADDRESS));
        return st;
    }
    
    public static Customer convertToCustomer(ResultSet rs) throws SQLException {
        Customer cs = new Customer();
        cs.setCustomer_id(rs.getString(CUS_COL_ID));
        cs.setCustomer_name(rs.getString(CUS_COL_CUSTOMER));
        cs.setDate_bitrh(convertStringToDate(rs.getString(CUS_COL_BIRTHDAY)));
        cs.setGender(rs.getInt(CUS_COL_GENDER));
        cs.setIdentity_card(rs.getString(CUS_COL_IDCARD));
        cs.setPhone_number(rs.getString(CUS_COL_PHONE));
        cs.setNationality(rs.getString(CUS_COL_NATIONALITY));
        logger.debug("Result: " + cs.toString());
        return cs;
    }
    
    public static Room convertToRoom(ResultSet rs) throws SQLException {
        Room room = new Room();
        room.setRoom_id(rs.getString(ROOM_COL_ID));
        RoomType type = new RoomType();
        type.setTypeId(rs.getString(ROOM_COL_TYPE));
        type.setPrice(rs.getDouble(6));
        type.setTypeName(rs.getString(5));
        room.setType(type);
        room.setRoom_rates(rs.getInt(ROOM_COL_RATE));
        return room;
    }
    
    public static RoomType convertToRoomType(ResultSet rs) throws SQLException {
        RoomType type = new RoomType();
        type.setTypeId(rs.getString(ROOM_TYPE_COL_ID));
        type.setTypeName(rs.getString(ROOM_TYPE_COL_NAME));
        type.setPrice(rs.getDouble(ROOM_TYPE_COL_PRICE));
        return type;
    }
    
    @SuppressWarnings("deprecation")
    public static Bill convertToBill(ResultSet rs) throws SQLException {
        Staff staff = new Staff();
        staff.setId(rs.getString(BILL_COL_STAFFID));
        staff.setName(rs.getString(5));

        Customer ct = new Customer();
        ct.setCustomer_id(rs.getString(6));
        ct.setCustomer_name(rs.getString(12));
        ct.setIdentity_card(rs.getString(13));
        ct.setPhone_number(rs.getString(14));
        
        RoomType type = new RoomType();
        type.setTypeId(rs.getString(15));
        type.setTypeName(rs.getString(17));
        type.setPrice(rs.getDouble(18));
        
        Room rm = new Room();
        rm.setRoom_id(rs.getString(7));
        rm.setType(type);
        rm.setRoom_rates(rs.getInt(16));
        
        Booking booking = new Booking();
        booking.setBook_id(rs.getString(BILL_COL_BOOKINGID));
        booking.setRoom(rm);
        booking.setCustomer(ct);
        booking.setRoom_id(rm.getRoom_id());
        booking.setDate_arrival(rs.getString(8));
        booking.setCheck_in(rs.getString(9));
        booking.setCheck_out(rs.getString(10));
        booking.setGuests(rs.getInt(11));
        String dateLeave = rs.getString(19);
        if (dateLeave == null) {
            Date d = new Date();
            d.setHours(12);
            dateLeave = convertDateToString(d);
        }
        booking.setDateLeave(dateLeave);
        
        Bill bill = new Bill();
        bill.setIdBill(rs.getString(1));
        bill.setCreatedDate(convertStringToDate(rs.getString(BILL_COL_BILL_DATE)));
        bill.setStaff(staff);
        bill.setBooking(booking);
        bill.setRoomPrice(type.getPrice());
        bill.setRoomType(type.getTypeName());
        bill.setName(ct.getCustomer_name());
        bill.setPhone(String.valueOf(ct.getPhone_number()));
        bill.setDateLeave(convertStringToDate(booking.getDateLeave()));
        bill.setDateArrival(convertStringToDate(booking.getDate_arrival()));
        bill.setIdCard(String.valueOf(ct.getIdentity_card()));
        
        logger.debug("Result: " + bill.toString());
        return bill;
    }
    
    public static Booking convertToBooking(ResultSet rs) throws SQLException {
        Booking booking = new Booking();
        booking.setBook_id(rs.getString(1));
        booking.setDate_arrival(rs.getString(4));
        booking.setCheck_in(rs.getString(6));
        booking.setCheck_out(rs.getString(7));
        booking.setGuests(rs.getInt(8));
        Customer cus = new Customer();
        cus.setCustomer_id(rs.getString(2));
        booking.setCustomer(cus);
        RoomType type = new RoomType();
        type.setTypeId(rs.getString(10));
        type.setTypeName(rs.getString(13));
        type.setPrice(rs.getDouble(14));
        Room room = new Room();
        room.setRoom_id(rs.getString(9));
        room.setRoom_rates(rs.getInt(11));
        room.setType(type);
        room.setRoom_type(type.getTypeName());
        booking.setRoom(room);
        logger.debug("Result: " + booking.toString());
        return booking;
    }
    
    public static Benefit convertToBenefit(ResultSet rs) throws SQLException {
        Benefit benefit = new Benefit();
        benefit.setBenefitId(rs.getInt(BENEFIT_COL_ID));
        benefit.setBenefitName(rs.getString(BENEFIT_COL_NAME));
        benefit.setBenefitPrice(rs.getDouble(BENEFIT_COL_PRICE));
        logger.debug("Result: " + benefit.toString());
        return benefit;
    }
    
    public static Date convertStringToDate(String value) {
        return convertStringToDate(value, false);
    }
    
    @SuppressWarnings("deprecation")
    public static Date convertStringToDate(String value, boolean isDay) {
        SimpleDateFormat format = new SimpleDateFormat(isDay ? PATTERN_ONLY_DAY : PATTERN_DATE);
        Date d = new Date();
        try {
            logger.debug(value);
            if (value == null) {
                d.setHours(12);
                return d;
            }
            d = format.parse(value);
            logger.debug(d.toString());
        } catch (ParseException e) {
            logger.error(e.getMessage(),e);
        }
        return d;
    }
    
    public static String convertDateToString(Date value) {
        SimpleDateFormat format = new SimpleDateFormat(PATTERN_DATE);
        return format.format(value);
    }
    
    public static String convertPrice(double price) {
        DecimalFormat format = new DecimalFormat("VND ###,###");
        return format.format(price);
    }
    
    public static Vector<RoomType> convertToVecRoomType(List<RoomType> types){
        Vector<RoomType> rts = new Vector<RoomType>();
        for (RoomType rt : types) {
            rts.add(rt);
        }
        return rts;
    }
    
    public static Vector<Room> convertToVecRoom(List<Room> types){
        Vector<Room> rts = new Vector<Room>();
        for (Room rt : types) {
            rts.add(rt);
        }
        return rts;
    }
    
    public static Vector<Staff> convertToVecStaff(List<Staff> types){
        Vector<Staff> rts = new Vector<Staff>();
        for (Staff rt : types) {
            rts.add(rt);
        }
        return rts;
    }
    
    public static Map<Integer, Benefit> convertBenefitToMap(List<Benefit> bs){
        Map<Integer, Benefit> mapBes = new HashMap<Integer, Benefit>();
        for (Benefit benefit : bs) {
            mapBes.put(benefit.getBenefitId(), benefit);
        }
        return mapBes;
    }
    
    public static double calTotalDays(Date arrival, Date leave) {
        long diff = leave.getTime() - arrival.getTime();
        logger.debug("Diff time " + leave.getTime() + " " + arrival.getTime() + " " + diff);
        double diffDays = (double) (diff / (24 * 60 * 60 * 1000)) + 1l;
        logger.debug("Diff Days " + diffDays);
        return diffDays;
    }
    
    public static void updateTotalPrice(Bill bill) {
        logger.debug("Bill " + bill.getIdBill());
        double totalPrice = bill.getRoomPrice() * calTotalDays(bill.getDateArrival(),bill.getDateLeave());
        
        for (Benefit benefit : bill.getBenefits()) {
            totalPrice += benefit.getBenefitPrice();
        }
        bill.setTotalPrice(totalPrice);
    }
    
    public static Date convertDateTimePickerToString(DateTimePicker picker) {
        LocalDateTime dt = picker.getDateTimePermissive();
        return Date.from(dt.atZone(ZONE_SYSTEM).toInstant());
    }
    
    public static void setDateTimePicker(DateTimePicker picker, Date date) {
        LocalDate optionalDate = date.toInstant().atZone(ZONE_SYSTEM).toLocalDate();;
        logger.debug(optionalDate.toString());
        picker.datePicker.setDate(optionalDate);
        LocalTime optionalTime = date.toInstant().atZone(ZONE_SYSTEM).toLocalTime();;
        logger.debug(optionalTime.toString());
        picker.timePicker.setTime(optionalTime);
    }
    
    public static Bill createBill() {
        Bill b =  new Bill();
        b.setRoomPrice(0l);
        Booking booking = new Booking();
        Customer customer = new Customer();
        List<Benefit> benefits = new ArrayList<Benefit>();
        booking.setCustomer(customer);
        b.setBenefits(benefits);
        return b;
    }
    
    public static String createBillID(Date createDate) {
        String strDate = String.valueOf(createDate.getTime());
        logger.debug("IDBILL--" + BILL_ID + strDate);
        return BILL_ID + strDate;
    }
}

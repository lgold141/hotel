package Receptionist;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class draw extends JPanel
{
    /**
	 * Author: Dang Van Hiep
	 */
	private static final long serialVersionUID = 1L;
	public void drawing()
    {
    	repaint();
    }
    public void paintComponent(Graphics g)
    {
    	super.paintComponent(g);
    	g.setColor(Color.RED);
    	g.fillRect(getHeight(),getWidth(),30,30);
    }
}

package Receptionist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class RoomList extends JFrame
{

	/**
	 * Author: Đặng Văn Hiệp
	 */
	private static final long serialVersionUID = 1L;
	JButton f1Btn,f2Btn,f3Btn,f4Btn,f5Btn,f6Btn,f7Btn,f8Btn;
	JButton r1Btn,r2Btn,r3Btn,r4Btn,r5Btn,r6Btn;
	JLabel emptyLbl,rentLbl,checkInLbl,checkOutLbl,noneLbl,bookLbl;
	JLabel bookNumLbl,checkInNumLbl,emptyNumLbl,rentNumLbl,checkOutNumLbl,noneNumLbl;
	JComboBox<String> filterComboBox;
	public RoomList(String title)
	{
		super(title);
		addControls();
		addEvents();
	}

	private void addEvents()
	{
		
		
	}

	private void addControls() 
	{
		Container con = this.getContentPane();
		JPanel mainPn = new JPanel(new BorderLayout());
		Color empty = new Color(128,211,221); 
		Color background = new Color(225,235,241);
		Font titleFont = new Font("Arial",Font.BOLD,15);
		
		JPanel westPn = new JPanel(); // default layout --> FlowLayout()
		westPn.setPreferredSize(new Dimension(253,0));
		westPn.setAlignmentX(CENTER_ALIGNMENT); // căn giữa các component trong westPn
		westPn.setBackground(background);
		
		f1Btn = new JButton("Floor 1");
		f1Btn.setPreferredSize(new Dimension(180,40));
		f1Btn.setBackground(Color.PINK);
		
		f2Btn = new JButton("Floor 2");
		f2Btn.setPreferredSize(new Dimension(180,40));
		f2Btn.setBackground(empty); // set màu button
		
		f3Btn = new JButton("Floor 3");
		f3Btn.setPreferredSize(new Dimension(180,40));
		f3Btn.setBackground(Color.gray);
		
		f4Btn = new JButton("Floor 4");
		f4Btn.setForeground(Color.BLACK); // set màu chữ
		f4Btn.setBackground(Color.PINK); // set màu button
		f4Btn.setPreferredSize(new Dimension(180,40));
		
		f5Btn = new JButton("Floor 5");
		f5Btn.setPreferredSize(new Dimension(180,40));
		f5Btn.setBackground(empty);
		
		f6Btn = new JButton("Floor 6");
		f6Btn.setPreferredSize(new Dimension(180,40));
		f6Btn.setBackground(Color.PINK);
		
		f7Btn = new JButton("Floor 7");
		f7Btn.setPreferredSize(new Dimension(180,40));
		f7Btn.setBackground(Color.green);
		
		f8Btn = new JButton("Floor 8");
		f8Btn.setPreferredSize(new Dimension(180,40));
		f8Btn.setBackground(Color.PINK);
		
		ImageIcon logo = new ImageIcon("src\\Icon\\logo.png");
		Image image1 = logo.getImage(); // transform it 
		Image newimg1 = image1.getScaledInstance(160,120,java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		logo = new ImageIcon(newimg1);  // transform it back
		JLabel logoLbl = new JLabel(logo);
		
		westPn.add(logoLbl);
		westPn.add(f1Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));// spacing theo vertical giữa các component là 10 px
		westPn.add(f2Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		westPn.add(f3Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		westPn.add(f4Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		westPn.add(f5Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		westPn.add(f6Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		westPn.add(f7Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		westPn.add(f8Btn);
		westPn.add(Box.createRigidArea(new Dimension(0,10)));
		
		JPanel centerPn = new JPanel(new FlowLayout(FlowLayout.CENTER,0,60));
		
		JPanel inforFPn = new JPanel(new GridLayout(3,2,15,1));
		inforFPn.setPreferredSize(new Dimension(300,430));
		
		Border borderFloor = BorderFactory.createLineBorder(Color.BLACK);
		TitledBorder borderTitleFloor = new TitledBorder(borderFloor,"Floor 1");
		borderTitleFloor.setTitleColor(Color.RED);
		borderTitleFloor.setTitleFont(titleFont); // set Font cho border title
		borderTitleFloor.setTitleJustification(TitledBorder.LEFT); // vị trí hiển thị border title
		inforFPn.setBorder(borderTitleFloor);
		
		r1Btn = new JButton("B101");
		r1Btn.setBackground(empty);
		r2Btn = new JButton("B102");
		r2Btn.setBackground(Color.green);
		r3Btn = new JButton("B103");
		r3Btn.setBackground(Color.pink);
		r4Btn = new JButton("B104");
		r4Btn.setBackground(Color.yellow);
		r5Btn = new JButton("B105");
		r5Btn.setBackground(Color.pink);
		r6Btn = new JButton("B106");
		r6Btn.setBackground(Color.red);
		//r6Btn.setVisible(false);
		
		inforFPn.add(r1Btn);
		inforFPn.add(r2Btn);
		inforFPn.add(r3Btn);
		inforFPn.add(r4Btn);
		inforFPn.add(r5Btn);
		inforFPn.add(r6Btn);
			
		centerPn.add(inforFPn);
		
		JPanel eastPn = new JPanel(new BorderLayout());
		eastPn.setPreferredSize(new Dimension(340,0));
		eastPn.setBackground(Color.WHITE);
		
		ImageIcon familySuite = new ImageIcon("src\\Icon\\FamilySuite.png");
		Image image = familySuite.getImage(); // transform it 
		Image newimg = image.getScaledInstance(320,300,java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		familySuite = new ImageIcon(newimg);  // transform it back
		JLabel inforRLbl = new JLabel(familySuite);
		eastPn.add(inforRLbl);
		
		JPanel northPn = new JPanel();
		northPn.setPreferredSize(new Dimension(0,35));
		//northPn.setBorder(BorderFactory.createMatteBorder(0,0,3,0,Color.WHITE)); 
		JLabel filterLbl = new JLabel("Filter ");

		String[] variable = {"View biển","View thành phố","Phòng trống","Phòng đến hạn Check-Out"};
		filterComboBox = new JComboBox<String>(variable);
		
		emptyLbl = new JLabel("Trống");
		rentLbl = new JLabel("Đang ở");
		bookLbl = new JLabel("Đặt cọc");
		checkInLbl = new JLabel("Sắp check-in");
		checkOutLbl = new JLabel("Đến hạn check-out");
		noneLbl = new JLabel("Không xác định");
		
		JPanel bookIconPn = new JPanel();
		bookIconPn.setPreferredSize(new Dimension(30,30));
		bookIconPn.setBackground(Color.pink);
		bookNumLbl = new JLabel("2");
		bookIconPn.add(bookNumLbl);
		
		JPanel checkOutIconPn = new JPanel();
		checkOutIconPn.setPreferredSize(new Dimension(30,30));
		checkOutIconPn.setBackground(Color.red);
		
		JPanel checkInIconPn = new JPanel();
		checkInIconPn.setPreferredSize(new Dimension(30,30));
		checkInIconPn.setBackground(Color.yellow);
		checkInNumLbl = new JLabel("12");
		checkInIconPn.add(checkInNumLbl);
		
		JPanel rentIconPn = new JPanel();
		rentIconPn.setPreferredSize(new Dimension(30,30));
		rentIconPn.setBackground(Color.green);
		
		JPanel emptyIconPn = new JPanel();
		emptyIconPn.setPreferredSize(new Dimension(30,30));
		emptyIconPn.setBackground(empty);
		
		JPanel noneIconPn = new JPanel();
		noneIconPn.setPreferredSize(new Dimension(30,30));
		noneIconPn.setBackground(Color.GRAY);
		
		northPn.add(filterLbl);
		northPn.add(filterComboBox);
		northPn.add(Box.createRigidArea(new Dimension(60,0)));
		northPn.add(emptyLbl);
		northPn.add(emptyIconPn);
		northPn.add(Box.createRigidArea(new Dimension(30,0)));
		northPn.add(rentLbl);
		northPn.add(rentIconPn);
		northPn.add(Box.createRigidArea(new Dimension(15,0)));
		northPn.add(checkInLbl);
		northPn.add(checkInIconPn);
		northPn.add(Box.createRigidArea(new Dimension(15,0)));
		northPn.add(checkOutLbl);
		northPn.add(checkOutIconPn);
		northPn.add(Box.createRigidArea(new Dimension(30,0)));
		northPn.add(bookLbl);
		northPn.add(bookIconPn);
		northPn.add(Box.createRigidArea(new Dimension(15,0)));
		northPn.add(noneLbl);
		northPn.add(noneIconPn);
		
		mainPn.add(westPn,BorderLayout.WEST);
		mainPn.add(centerPn,BorderLayout.CENTER);
		mainPn.add(eastPn,BorderLayout.EAST);
		mainPn.add(northPn,BorderLayout.NORTH);
		
		con.add(mainPn);
	}
	
	protected void showWindow()
	{
		 this.setBounds(20,40,1125,650);
		 this.setLocationRelativeTo(null); // vị trí Frame nằm ở vị trí tương đối với null
	     this.setResizable(false); // không được chỉnh sửa size của Frame
	     this.setVisible(true); //cho phép hiển thị Frame
	}

	public static void main(String args[])
	{
		RoomList DanhSachPhong = new RoomList("Room List");
		DanhSachPhong.showWindow();
	}
}

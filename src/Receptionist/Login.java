package Receptionist;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import Receptionist.SpringUtilities;

public class Login extends JFrame
{
    /**
	 * Author: Đặng Văn Hiệp
	 */
	private static final long serialVersionUID = 1L;
    JTextField usernameTxt;
    JPasswordField passwordPsw;
    JLabel usernameLbl, passwordLbl;
	JButton okBtn, cancelBtn;
	
	public Login(String title)
    {
    	super(title);
    	addControls();
    	addEvents();
    }
	
	protected void addEvents() 
	{
		
		
	}

	protected void addControls() 
	{
		Container con = this.getContentPane();
		JPanel mainPn = new JPanel(new BorderLayout());
		Color backgroundP = new Color(225,235,241);
		Color title = new Color(49,51,53);
		JPanel northPn = new JPanel();
		JLabel lblTieuDe= new JLabel("Login");
        Font fontTieuDe= new Font("Arial", Font.BOLD, 30);
        lblTieuDe.setForeground(title);
        lblTieuDe.setFont(fontTieuDe);
        northPn.add(lblTieuDe); 
		
		JPanel westPn = new JPanel(new BorderLayout());
		westPn.setPreferredSize(new Dimension(210,0)); // setSize
		
		ImageIcon imageIcon = new ImageIcon("src\\Icon\\second.jpg");
		Image image = imageIcon.getImage(); // transform it 
		Image newimg = image.getScaledInstance(210,250,java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		imageIcon = new ImageIcon(newimg);  // transform it back
		
		JLabel iconLbl = new JLabel();
		iconLbl.setIcon(imageIcon);
	    westPn.add(iconLbl);
		
		//create and populate the panel
		JPanel Pn = new JPanel(new SpringLayout());
		Pn.setPreferredSize(new Dimension(260,80)); // setSize
		Pn.setBackground(backgroundP);
		
		usernameLbl = new JLabel("Username");
		Pn.add(usernameLbl);
		usernameTxt  = new JTextField(10);
		usernameLbl.setLabelFor(usernameTxt);
		Pn.add(usernameTxt);
		
		passwordLbl = new JLabel("Password");
		Pn.add(passwordLbl);
		passwordPsw  = new JPasswordField(10);
		passwordLbl.setLabelFor(passwordPsw);
		Pn.add(passwordPsw);
		
		//Lay out the panel.
		SpringUtilities.makeCompactGrid(Pn,
		                                2, 2, //rows, cols
		                                6, 6,        //initX, initY
		                                6, 6);       //xPad, yPad
		JPanel centerPn = new JPanel();
		centerPn.setBackground(backgroundP);
		centerPn.add(Pn);
		
		JPanel southPn = new JPanel();
		southPn.setPreferredSize(new Dimension(0,45));
		okBtn = new JButton("OK");
		cancelBtn = new JButton("Cancel");
		southPn.add(okBtn);
		southPn.add(cancelBtn);
	
		mainPn.add(northPn,BorderLayout.NORTH);
		mainPn.add(westPn,BorderLayout.WEST);
		mainPn.add(centerPn,BorderLayout.CENTER);
		mainPn.add(southPn,BorderLayout.SOUTH);
		
		con.add(mainPn);
	}
	
	protected void showWindow()
	{
		 this.setSize(600,300);
	     this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	     this.setLocationRelativeTo(null);
	     this.setResizable(false);
	     this.setVisible(true);
	}

	public static void main(String args[])
	{
		Login loginFr = new Login("Login Form");
		loginFr.showWindow();
	}
}
package Receptionist;


import java.awt.Color;
import java.awt.Container;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JLayeredPane;

public class ReceptionistMenu extends JFrame
{
	/**
	 * @author: Dang Van Hiep
	 */
	private static final long serialVersionUID = 1L;

	public ReceptionistMenu(String title)
	{
		super(title);
		addControls();
		addEvents();
	}
	
	private void addEvents() 
	{
		
	}

	private void addControls() 
	{
		Container con = this.getContentPane();
		setBackground(SystemColor.activeCaption);
		getContentPane().setLayout(null);
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(0,0,1000,600);
		getContentPane().add(layeredPane);
		
		JLabel backgroundLbl = new JLabel("");
		backgroundLbl.setIcon(new ImageIcon(ReceptionistMenu.class.getResource("/Icon/hotel2.jpg")));
		layeredPane.setLayer(backgroundLbl, 1);
		backgroundLbl.setBounds(0,0,1000,600);
		layeredPane.add(backgroundLbl);
		
		JButton checkInBtn = new JButton("Check-In");
		checkInBtn.setIcon(new ImageIcon(ReceptionistMenu.class.getResource("/Icon/checkin.png")));
		layeredPane.setLayer(checkInBtn, 2);
		checkInBtn.setBounds(100,150,170,150);
		layeredPane.add(checkInBtn);
		checkInBtn.setToolTipText("Check-Out");
		checkInBtn.setFocusable(false);
		checkInBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		checkInBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		
		JButton checkOutBtn = new JButton("Check-Out");
		checkOutBtn.setIcon(new ImageIcon(ReceptionistMenu.class.getResource("/Icon/Checkout&Payment.png")));
		layeredPane.setLayer(checkOutBtn, 3);
		layeredPane.add(checkOutBtn);
		checkOutBtn.setBounds(100,300,170,150);
		layeredPane.add(checkOutBtn);
		checkOutBtn.setToolTipText("Check-Out");
		checkOutBtn.setFocusable(false);
		checkOutBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		checkOutBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		
		JButton bookingBtn = new JButton("Booking");
		bookingBtn.setIcon(new ImageIcon(ReceptionistMenu.class.getResource("/Icon/Booking.png")));
		layeredPane.setLayer(bookingBtn, 2);
		bookingBtn.setBounds(270,150,170,300);
		layeredPane.add(bookingBtn);
		bookingBtn.setToolTipText("Book a room");
		bookingBtn.setFocusable(false);
		bookingBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		bookingBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

		
		JButton serviceBtn = new JButton("Service");
		serviceBtn.setIcon(new ImageIcon(ReceptionistMenu.class.getResource("/Icon/foods.png")));
		layeredPane.setLayer(serviceBtn, 2);
		serviceBtn.setBounds(440,150,300,150);
		layeredPane.add(serviceBtn);
		serviceBtn.setToolTipText("Service of Hotel");
		serviceBtn.setFocusable(false);
		serviceBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		serviceBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		
		JButton btnDoanhThu = new JButton("Revenue");
		btnDoanhThu.setIcon(new ImageIcon(ReceptionistMenu.class.getResource("/Icon/LogBook_1.png")));
		layeredPane.setLayer(btnDoanhThu, 2);
		btnDoanhThu.setBounds(740,150,170,150);
		layeredPane.add(btnDoanhThu);
		btnDoanhThu.setToolTipText("Revenue Statistics");
		btnDoanhThu.setFocusable(false);
		btnDoanhThu.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		btnDoanhThu.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		
		ImageIcon signOut = new ImageIcon(ReceptionistMenu.class.getResource("/Icon/SignOut.png"));
		Image image1 = signOut.getImage(); // transform it 
		Image newimg1 = image1.getScaledInstance(32,32,java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		signOut = new ImageIcon(newimg1);  // transform it back
		
		JButton logoutBtn = new JButton();
		logoutBtn.setIcon(signOut);
		logoutBtn.setToolTipText("Sign-Out");
        logoutBtn.setBackground(Color.GRAY);
		logoutBtn.setBounds(920,20,50,50);
		logoutBtn.setFocusable(false);
		logoutBtn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		logoutBtn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		layeredPane.setLayer(logoutBtn, 2);
		layeredPane.add(logoutBtn);

		ImageIcon logo = new ImageIcon(ReceptionistMenu.class.getResource("/Icon/logo.png"));
		Image image = logo.getImage(); // transform it 
		Image newimg = image.getScaledInstance(230,140,java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		logo = new ImageIcon(newimg);  // transform it back
		
		JLabel iconLbl = new JLabel(logo);
		iconLbl.setBounds(0,0,235,140);
		layeredPane.setLayer(iconLbl, 2);
		layeredPane.add(iconLbl);
	}
	
	protected void showWindow()
	{	
		 this.setBounds(190,80,1000,600);
	     this.setDefaultCloseOperation(EXIT_ON_CLOSE); // thoát hẳn khỏi programming khi đóng Frame
	     this.setResizable(false); // không được chỉnh sửa size của Frame
	     this.setVisible(true); //cho phép hiển thị Frame
	}

	public static void main(String args[])
	{
		ReceptionistMenu menuFr = new ReceptionistMenu("Menu");
		menuFr.showWindow();
	}
}

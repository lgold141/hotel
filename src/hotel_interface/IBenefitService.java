package hotel_interface;

import java.util.List;

import hotel_model.Benefit;

public interface IBenefitService {
    public boolean createBenefit(String name, double price);

    public int deleteBenefit(Integer id);

    public int modifyBenefit(Integer id, String newName, double newPrice);

    public Benefit getBenefit(Integer id);

    public List<Benefit> getBenefits();
}

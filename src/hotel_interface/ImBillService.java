package hotel_interface;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hotel_util.ModelUtil.*;
import hotel_connect.BenefitSQLService;
import hotel_connect.BillSQLService;
import hotel_connect.BookingService;
import hotel_connect.CusBenefitsSQLService;
import hotel_connect.CustomerSQLService;
import hotel_connect.RoomSQLService;
import hotel_connect.RoomTypeSQLService;
import hotel_connect.StaffSQLService;
import hotel_model.Benefit;
import hotel_model.Bill;
import hotel_model.Booking;
import hotel_model.Customer;
import hotel_model.Room;
import hotel_model.RoomType;
import hotel_model.Staff;

public class ImBillService implements IBillService {
    
    private BillSQLService bill;
    private CusBenefitsSQLService cus;
    
    public ImBillService() {
        bill = new BillSQLService();
        cus = new CusBenefitsSQLService();
    }

    @Override
    public Map<String, Bill> getAllBills() {
        List<Bill> bills = bill.getAllBill();
        Map<String, Bill> map = new HashMap<String, Bill>();
        for (Bill bill : bills) {
            updateBenefits(bill);
            updateTotalPrice(bill);
            map.put(bill.getIdBill(), bill);
        }
        return map;
    }


    private void updateBenefits(Bill bill) {
        Booking booking = bill.getBooking();
        Customer customer = booking.getCustomer();
        List<Benefit> benefits = cus.getBenefitsOfCustomer(customer.getCustomer_id());
        bill.setBenefits(benefits);
    }
    
    @Override
    public int addBill(Bill tmpBill) {
        logger.debug("Adding Bill " + tmpBill.getIdBill());
        Customer orgCus = tmpBill.getBooking().getCustomer();
        List<String> sqls = new ArrayList<String>();
        Booking tmpBook = tmpBill.getBooking();
        sqls.add(String.format(BookingService.UPDATE_BOOKING_2, tmpBook.getDate_arrival(),
                tmpBook.getDateLeave(), tmpBook.getGuests(), tmpBill.getBooking().getRoom_id() ,tmpBook.getBook_id()));
        sqls.add(String.format(CusBenefitsSQLService.DEL_BENEFITS_OF_CUSTOMER_BY_CUS, orgCus.getCustomer_id()));
        Date d = new Date();
        for (Benefit benefit : tmpBill.getBenefits()) {
            sqls.add(String.format(CusBenefitsSQLService.ADD_BENEFITS_OF_CUSTOMER_2, orgCus.getCustomer_id(),benefit.getBenefitId(),convertDateToString(d)));
        }
        sqls.add(String.format(BillSQLService.ADD_BILL_2, tmpBill.getIdBill(), tmpBill.getBooking().getBook_id(),
                convertDateToString(tmpBill.getCreatedDate()), tmpBill.getStaff().getId()));
        return bill.executeMultiSQL(sqls);
    }

    @SuppressWarnings("unlikely-arg-type")
    @Override
    public int updateBill(Bill tmpBill, Bill orgBill) {
        List<String> sqlUpdateBill = new ArrayList<String>();
        Customer tmpCus = tmpBill.getBooking().getCustomer();
        Customer orgCus = orgBill.getBooking().getCustomer();
        if (!tmpCus.getCustomer_name().equals(orgCus.getCustomer_name())
                || !tmpCus.getIdentity_card().equals(orgCus.getIdentity_card())
                || !tmpCus.getPhone_number().equals(orgCus.getPhone_number())) {
            sqlUpdateBill.add(String.format(CustomerSQLService.UPDATE_CUSTOMER_2,tmpCus.getCustomer_name(), tmpCus.getIdentity_card(),
                    tmpCus.getPhone_number(), orgCus.getCustomer_id()));
        }
        Booking tmpBook = tmpBill.getBooking();
        Booking orgBook = orgBill.getBooking();
        if (!tmpBook.getDate_arrival().equals(orgBook.getDate_arrival()) 
                || !tmpBook.getDateLeave().equals(orgBook.getDateLeave())
                || (tmpBook.getGuests()!= orgBook.getGuests()
                || tmpBill.getRoomPrice()!= orgBill.getRoomPrice())) {
            sqlUpdateBill.add(String.format(BookingService.UPDATE_BOOKING_2, tmpBook.getDate_arrival(),
                    tmpBook.getDateLeave(), tmpBook.getGuests(), tmpBill.getBooking().getRoom_id() ,orgBook.getBook_id()));
        }
        if (!tmpBill.getStaff().equals(orgBill.getStaff().getName())) {
            sqlUpdateBill.add(String.format(BillSQLService.UPDATE_BILL_2,tmpBill.getStaff().getId(), orgBill.getIdBill()));
        }
        sqlUpdateBill.add(String.format(CusBenefitsSQLService.DEL_BENEFITS_OF_CUSTOMER_BY_CUS, orgCus.getCustomer_id()));
        Date d = new Date();
        for (Benefit benefit : tmpBill.getBenefits()) {
            sqlUpdateBill.add(String.format(CusBenefitsSQLService.ADD_BENEFITS_OF_CUSTOMER_2, orgCus.getCustomer_id(),benefit.getBenefitId(),convertDateToString(d)));
        }
        return bill.executeMultiSQL(sqlUpdateBill);
    }

    @Override
    public int deleteBill(String billID) {
        return bill.deleteBillByID(billID);
    }

    @Override
    public Bill getBill(String id) {
        Bill b = bill.findBillByID(id);
        updateBenefits(b);
        updateTotalPrice(b);
        return b;
    }

    @Override
    public List<RoomType> getAllRoomTypes() {
        RoomTypeSQLService rt = new RoomTypeSQLService();
        return rt.getAllRoomTypes();
    }

    @Override
    public List<Staff> getAllStaffs() {
        StaffSQLService st = new StaffSQLService();
        return st.getAllStaffs();
    }

    @Override
    public List<Customer> getAllCustomers() {
        CustomerSQLService cs = new CustomerSQLService();
        return cs.getAllCustomers();
    }

    @Override
    public List<Benefit> getAllBenefits() {
        BenefitSQLService bn = new BenefitSQLService();
        return bn.getAllBenefits();
    }

    @Override
    public List<Room> getAllRooms(String bookID) {
        RoomSQLService r = new RoomSQLService();
        BookingService b = new BookingService();
        List<Booking> books = b.getAllBooking();
        List<Room> ro = new ArrayList<Room>();
        List<Room> rooms = r.getAllRooms();
        for (Room room : rooms) {
            boolean isExist = false;
            for (Booking booking : books) {
                if (bookID != null) {
                    if (booking.getBook_id().equals(bookID) && booking.getRoom_id().equals(room.getRoom_id())) {
                        isExist = false;
                        break;
                    }
                }
                
                if (booking.getRoom_id().equals(room.getRoom_id())) {
                    isExist = true;
                    break;
                }
                
            }
            if (!isExist) {
                ro.add(room);
            }
        }
        return ro;
    }

    @Override
    public Booking findBookingCustomer(String customerID) {
        BookingService s = new BookingService();
        return s.findBookingByCustomerID(customerID);
    }

    @Override
    public List<Benefit> getBenefitByCustomer(String customerID) {
        return cus.getBenefitsOfCustomer(customerID);
    }
}

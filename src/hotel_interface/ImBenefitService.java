package hotel_interface;

import java.util.List;

import hotel_connect.BenefitSQLService;
import hotel_model.Benefit;

public class ImBenefitService implements IBenefitService {
    private BenefitSQLService service;

    public ImBenefitService() {
        service = new BenefitSQLService();
    }

    @Override
    public boolean createBenefit(String name, double price) {
        return service.addBenefit(name, price);
    }

    @Override
    public int deleteBenefit(Integer id) {
        return service.deleteBenefitByName(id);
    }

    @Override
    public int modifyBenefit(Integer id, String newName, double newPrice) {
        return service.updateBenefit(id, newName, newPrice);
    }

    @Override
    public List<Benefit> getBenefits() {
        return service.getAllBenefits();
    }

    @Override
    public Benefit getBenefit(Integer id) {
        return service.findBenefitByID(id);
    }

}

package hotel_interface;

import java.util.List;
import java.util.Map;

import hotel_model.Benefit;
import hotel_model.Bill;
import hotel_model.Booking;
import hotel_model.Customer;
import hotel_model.Room;
import hotel_model.RoomType;
import hotel_model.Staff;

public interface IBillService {
    Map<String, Bill> getAllBills();
    Bill getBill(String id);
    int updateBill(Bill tmpBill, Bill orgBill);
    int addBill(Bill bill);
    int deleteBill(String billID);
    List<RoomType> getAllRoomTypes();
    List<Room> getAllRooms(String bookID);
    List<Staff> getAllStaffs();
    List<Customer> getAllCustomers();
    List<Benefit> getAllBenefits();
    Booking findBookingCustomer(String customerID);
    List<Benefit> getBenefitByCustomer(String customerID);
}

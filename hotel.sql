-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 28, 2020 lúc 05:41 AM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `hotel`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `benefit`
--

CREATE TABLE `benefit` (
  `benefit_id` int(11) NOT NULL,
  `benefit_name` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `benefit_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `benefit`
--

INSERT INTO `benefit` (`benefit_id`, `benefit_name`, `benefit_price`) VALUES
(10, 'Ice-creams', 20000),
(11, 'Cocktails', 50000),
(12, 'Internet', 60000),
(13, 'Candy', 5000),
(14, 'Game', 100000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill`
--

CREATE TABLE `bill` (
  `bill_id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `book_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `bill_date` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `staff_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `bill`
--

INSERT INTO `bill` (`bill_id`, `book_id`, `bill_date`, `staff_id`) VALUES
('BILL1', 'BOOK1', '2020-05-10 09:01:07', 'STAFF1'),
('BILL2', 'BOOK2', '2020-05-12 10:48:07', 'STAFF2'),
('BILL3', 'BOOK3', '2020-06-10 16:20:07', 'STAFF4'),
('BILL4', 'BOOK4', '2020-06-20 16:48:07', 'STAFF2'),
('BILL5', 'BOOK5', '2020-06-20 17:50:48', 'STAFF1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `booking`
--

CREATE TABLE `booking` (
  `book_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `customer_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `room_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `date_arrival` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `date_leave` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `check_in` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `check_out` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `guests` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `booking`
--

INSERT INTO `booking` (`book_id`, `customer_id`, `room_id`, `date_arrival`, `date_leave`, `check_in`, `check_out`, `guests`) VALUES
('BOOK1', 'CUS1', 'ROOM1', '2020-05-20 12:00:00', '2020-06-27 12:46:00', 'YES', 'NO', 3),
('BOOK2', 'CUS2', 'ROOM2', '2020-06-20 16:04:00', '2020-06-30 12:00:00', 'YES', 'NO', 1),
('BOOK3', 'CUS3', 'ROOM3', '2020-06-20 16:04:00', '2020-06-30 01:30:00', 'YES', 'NO', 3),
('BOOK4', 'CUS4', 'ROOM4', '2020-06-20 16:04:00', '2020-06-30 12:56:00', 'YES', 'NO', 1),
('BOOK5', 'CUS5', 'ROOM5', '2020-06-20 16:04:16', NULL, 'YES', 'NO', 2),
('BOOK6', 'CUS6', 'ROOM6', '2020-06-20 16:04:16', NULL, 'YES', 'NO', 2),
('BOOK7', 'CUS7', 'ROOM10', '2020-06-20 16:04:00', '2020-06-28 12:38:00', 'Yes', 'No', 2),
('BOOK8', 'CUS8', 'ROOM7', '2020-06-20 16:04:00', '2020-06-28 12:37:00', 'Yes', 'No', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `customer_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `customer_name` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `date_birth` date DEFAULT NULL,
  `sex` int(11) NOT NULL,
  `identity_card` int(11) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `date_birth`, `sex`, `identity_card`, `phone_number`, `nationality`) VALUES
('CUS1', 'Test13', '1990-06-20', 0, 755187, '2087923778', 'VietNam'),
('CUS2', 'Test2', '1989-07-02', 1, 787878, '7894561278', 'England'),
('CUS3', 'Test3', '2000-10-20', 0, 123789, '7894561212', 'Italy'),
('CUS4', 'Test4', '1998-10-16', 1, 567889, '2348978789', 'Laos'),
('CUS5', 'Test5', '2020-06-20', 1, 784545, '2879224', 'VietNam'),
('CUS6', 'Sasuke', '1991-06-20', 0, 569997, '59782789', 'Japan'),
('CUS7', 'Test7', '2020-06-03', 1, 123547, '1234567878', 'VietNam'),
('CUS8', 'Test8', '2020-06-03', 1, 123547789, '9876543210', 'VietNam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cus_benefit`
--

CREATE TABLE `cus_benefit` (
  `customer_benefit_id` int(11) NOT NULL,
  `customer_id` varchar(25) COLLATE utf8_vietnamese_ci NOT NULL,
  `benefit_id` int(11) NOT NULL,
  `date` varchar(25) COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `cus_benefit`
--

INSERT INTO `cus_benefit` (`customer_benefit_id`, `customer_id`, `benefit_id`, `date`) VALUES
(47, 'CUS1', 10, '2020-06-27 15:08:31'),
(48, 'CUS2', 11, '2020-06-27 16:00:41'),
(56, 'CUS3', 10, '2020-06-27 16:23:33'),
(57, 'CUS4', 14, '2020-06-27 17:56:44'),
(58, 'CUS4', 12, '2020-06-27 17:56:44'),
(76, 'CUS8', 13, '2020-06-28 10:37:25'),
(77, 'CUS8', 12, '2020-06-28 10:37:25'),
(78, 'CUS7', 14, '2020-06-28 10:38:33'),
(79, 'CUS7', 12, '2020-06-28 10:38:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `room`
--

CREATE TABLE `room` (
  `room_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `type_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `room_rate` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `room`
--

INSERT INTO `room` (`room_id`, `type_id`, `room_rate`) VALUES
('ROOM1', 'TYPE3', 4),
('ROOM10', 'TYPE5', 6),
('ROOM2', 'TYPE3', 4),
('ROOM3', 'TYPE3', 4),
('ROOM4', 'TYPE4', 4),
('ROOM5', 'TYPE5', 4),
('ROOM6', 'TYPE6', 4),
('ROOM7', 'TYPE4', 4),
('ROOM8', 'TYPE2', 4),
('ROOM9', 'TYPE2', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `room_type`
--

CREATE TABLE `room_type` (
  `type_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `type_name` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `room_type`
--

INSERT INTO `room_type` (`type_id`, `type_name`, `price`) VALUES
('TYPE1', 'Single', 100000),
('TYPE2', 'Double', 150000),
('TYPE3', 'Triple', 200000),
('TYPE4', 'Quad', 250000),
('TYPE5', 'Queen', 300000),
('TYPE6', 'Twin', 350000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `staff`
--

CREATE TABLE `staff` (
  `staff_id` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `staff_name` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `staff_password` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `staff_birthday` date DEFAULT NULL,
  `staff_idcard` int(11) NOT NULL,
  `staff_gender` int(11) NOT NULL,
  `staff_address` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_name`, `staff_password`, `staff_birthday`, `staff_idcard`, `staff_gender`, `staff_address`) VALUES
('STAFF1', 'Staff1', '123456', '1987-06-20', 12345678, 1, 'Address 1'),
('STAFF2', 'Staff2', '789456', '1999-10-20', 45678912, 0, 'Address 2'),
('STAFF3', 'Staff3', '123456', '2020-06-20', 12345678, 1, 'Address 3'),
('STAFF4', 'Staff4', '456123', '2000-01-10', 12002787, 0, 'Address 4');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `benefit`
--
ALTER TABLE `benefit`
  ADD PRIMARY KEY (`benefit_id`);

--
-- Chỉ mục cho bảng `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`bill_id`),
  ADD KEY `book_foreignkey` (`book_id`) USING BTREE,
  ADD KEY `staff_foreignkey` (`staff_id`);

--
-- Chỉ mục cho bảng `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `custom_foreignkey` (`customer_id`),
  ADD KEY `room_foreignkey` (`room_id`) USING BTREE;

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`) USING BTREE;

--
-- Chỉ mục cho bảng `cus_benefit`
--
ALTER TABLE `cus_benefit`
  ADD PRIMARY KEY (`customer_benefit_id`) USING BTREE,
  ADD KEY `customer_foreignkey` (`customer_id`),
  ADD KEY `benefit_foreignkey` (`benefit_id`) USING BTREE;

--
-- Chỉ mục cho bảng `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`room_id`),
  ADD KEY `roomtype_foreignkey` (`type_id`);

--
-- Chỉ mục cho bảng `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Chỉ mục cho bảng `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `benefit`
--
ALTER TABLE `benefit`
  MODIFY `benefit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `cus_benefit`
--
ALTER TABLE `cus_benefit`
  MODIFY `customer_benefit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`),
  ADD CONSTRAINT `bill_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `booking` (`book_id`);

--
-- Các ràng buộc cho bảng `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`room_id`);

--
-- Các ràng buộc cho bảng `cus_benefit`
--
ALTER TABLE `cus_benefit`
  ADD CONSTRAINT `cus_benefit_ibfk_1` FOREIGN KEY (`benefit_id`) REFERENCES `benefit` (`benefit_id`),
  ADD CONSTRAINT `cus_benefit_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Các ràng buộc cho bảng `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `room_type` (`type_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
